<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		<meta http-equiv="X-UA-Compatible" content="ie=edge">
		<spring:url value="/resources" var="urlPublic" />
		<spring:url value="/vet/modificar" var="urlVet" />
		<spring:url value="/grilla/save" var="urlForm" />
		<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
		<!-- Bootstrap -->
	    <link href='https://fonts.googleapis.com/css?family=Maven+Pro' rel='stylesheet' type='text/css'>
	    <link href="${urlPublic}/js/style.css" rel="stylesheet">
	
	</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>${titulo}</h2>
		<p class="lead">Aqui usted podr� administrar la Grilla de Turnos</p>
	</div>
	<span title="Eliminar"><a href="${urlVet}/${veterinaria.id}">Editar</a></span>

	<!-- row grilla -->
	<form:form class="needs-validation" action="${urlForm}" method="POST" modelAttribute="grilla">
	            <form:input type="hidden" class="form-control" path="vete.id" required="required" value="1"/>
				<form:input type="hidden" class="form-control" path="id" required="required" value="${grilla.id}"/>
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token }" />
				
	            <label>Descripci�n:</label>
	            <form:textarea class="form-control" path="descripcion" id="descripcion" rows="3" value="${grilla.descripcion}"/>
<!-- 	            <label>D�as:</label> -->
<!-- 	            <div id="days-list" class="col-sm-12"> -->
<!-- 	               <a data-day="1" class="day-option">Lunes</a> -->
<!-- 	               <a data-day="2" class="day-option">Martes</a> -->
<!-- 	               <a data-day="3" class="day-option">Mi�rcoles</a> -->
<!-- 	               <a data-day="4" class="day-option">Jueves</a> -->
<!-- 	               <a data-day="5" class="day-option">Viernes</a> -->
<!-- 	               <a data-day="6" class="day-option">S�bado</a> -->
<!-- 	               <a data-day="7" class="day-option">Domingo</a> -->
<!-- 	            </div> -->
<!-- 	            <input id="days-chose" class="form-control" type="text" name="days" > -->
	            <label>Inicio:</label>
	            <input class="form-control" type="number" id="hora_ini" name="hora_ini" value="${grilla.hora_ini}">
	            <label>Final:</label>
	            <input class="form-control" type="number" id="hora_fin" name="hora_fin" value="${grilla.hora_fin}">
	            <label>Dividir Entre:</label>
	            <select class="form-control" name="horario">
	                <option></option>
	                <option value="30">30 Minutos</option>
	                <option value="45">45 minutos</option>
	                <option value="60">1 Hora</option>
	                <option selected value="${grilla.horario}">${grilla.horario}</option>
	            </select>
	            <button class="btn btn-primary btn-lg btn-block" type="submit"
					value="Guardar">Guardar</button>
	         </form:form>
<!-- 	<div class="row" style="padding-left: 285px"> -->
<!-- 		<div class="col-md-8 order-md-1"> -->
<!-- 			<table class="table table-striped table-sm"> -->
<!-- 					<thead> -->
<!-- 						<tr> -->
<!-- 							<th>Lunes</th> -->
<!-- 							<th>Martes</th> -->
<!-- 							<th>Miercoles</th> -->
<!-- 							<th>Jueves</th> -->
<!-- 							<th>Viernes</th> -->
<!-- 						</tr> -->
<!-- 					</thead> -->
<!-- 					<tbody> -->
<!-- 						<tr> -->
<%-- 							<c:forEach> --%>
<!-- 							<td></td> -->
<!-- 							<td></td> -->
<%-- 							</c:forEach> --%>
<!-- 						</tr> -->
<!-- 					</tbody> -->
<!-- 				</table> -->
<!-- 		</div> -->

<!-- 	</div>row grilla -->
</body>
	<jsp:include page="../includes/footer.jsp" />
</html>