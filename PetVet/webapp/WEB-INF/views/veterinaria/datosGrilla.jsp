<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/vet/modificar" var="urlVet" />
<spring:url value="/grilla/save" var="urlForm" />
<spring:url value="/grilla/buscar" var="urlBuscar" />
<spring:url value="/grilla" var="urlGrilla" />
<spring:url value="/turno" var="urlTurno" />

<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap -->
<link href='https://fonts.googleapis.com/css?family=Maven+Pro'
	rel='stylesheet' type='text/css'>
<link href="${urlPublic}/js/style.css" rel="stylesheet">

</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>${titulo}</h2>
		<p class="lead">Aqui usted podr� administrar la Grilla de Turnos</p>
	</div>
	<div class="row p-3">
		<div class="col p-3 clearfix">
			<h4 class="float-right">
				<b><span title="Editar"><a
						href="${urlVet}/${veterinaria.id}">${veterinaria.nombre}</a></span></b>
			</h4>
			<br> <br> <small class="float-right">Ubicado en: <b>${veterinaria.direccion}</b>
			</small><br> <small class="float-right">P�gina web: <b>${veterinaria.pagina_web}</b>
			</small><br>
			<%-- 		       <span title="Crear"><a type="button" class="btn btn-secondary" href="${urlGrilla}/alta/${veterinaria.id}"/>Crear Grilla</a></span> --%>
		</div>
		<div class="col p-3 clearfix">
			<small class="float-left"><img
				src="${urlPublic}/images/${veterinaria.imagen}"
				style="width: 200px;"></small>
		</div>
	</div>
	<div class="py-5 text-center">
		<p class="lead">A continuacion se encuentran todos los turnos
			disponibles de la veterinaria ${veterinaria.nombre}</p>
	</div>

	<div class="row py-5">
		<form:form class="needs-validation mx-auto"
			action="${urlBuscar}/${veterinaria.id}" method="post"
			modelAttribute="turno">
			<form:input type="hidden" class="form-control" path="id" id="id"
				required="required" value="${turno.id}" />
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token }" />
				<label>Seleccione la fecha:</label>
				<form:input class="form-control" type="date" id="fecha" path="fecha" style="width:150px;"
					value="${turno.fecha}" /> <br>
				<button class="btn btn-primary btn-lg btn-block " type="submit" style="width:150px;"
					value="Guardar">Buscar</button>
		</form:form>
	</div>
	<div class="row p-3 ">
		<div class="col p-3 mx auto" style="width: 1000px">
			<table class="table table-striped" id="myTable">
				<thead>
					<tr>
						<th scope="col">Hora</th>
						<th scope="col">Fecha: ${fecha}</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="i" begin="0" end="${horarios.size()-1}">
						<tr>
							<th scope="row">${horarios.get(i)}</th>
							<!-- Lun -->
							<td><c:set var="id" value="${turnos.get(i).id}" /> <c:choose>
									<c:when test="${id == 0}">
										<form:form class="needs-validation" action="${urlTurno}/alta"
											method="post" modelAttribute="turno">
											<form:input type="hidden" class="form-control" path="id" id="id" required="required" value="${turnos.get(i).id}" />
											<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token }" />
											<form:input type="hidden" class="form-control" path="duenoMascota.id" id="duenoMascota.id"
												required="required" value="${user.id}" />
											<form:input type="hidden" class="form-control" path="vete.id" id="vete.id" required="required" value="${veterinaria.id}" />
											<form:input type="hidden" class="form-control" path="fecha" id="fecha" required="required" value="${fecha}" />
											<form:input type="hidden" class="form-control" path="hora" id="hora" required="required" value="${turnos.get(i).hora}" />
											<button class="btn btn-primary btn-lg" type="submit" value="Guardar">Solicitar Turno</button>
										</form:form>
										
									</c:when>
									<c:otherwise>
										<span>(${i}) - ${turnos.get(i).id}
											${turnos.get(i).estado} ${turnos.get(i).duenoMascota.nombre}
											${turnos.get(i).duenoMascota.apellido} <br>
										</span>
									</c:otherwise>
								</c:choose></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>