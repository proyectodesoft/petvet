<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/vet" var="urlVet" />
<spring:url value="/grilla" var="urlGrilla" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>Veterinarias</title>

</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>Listado de Tiendas Veterinarias - ${user.rol.descripcion}</h2>
		<p class="lead">Aqui usted podr� listar todas las veterinarias del
			sistema</p>
	</div>
	<div>
		<c:if test="${msg !=null }">
			<div class='alert alert-success' role='alert'>${msg}</div>
		</c:if>
	</div>
	<c:set var="val" value="${user.rol.descripcion}" />
	<c:choose>
		<c:when test="${val == 'ADMIN'}">
			<span title="Alta"><a href="${urlVet}/alta"><i
					class="fa fa-plus"></i></a></span>
		</c:when>
	</c:choose>
	<div class="table-responsive">
		<table class="table table-striped table-sm" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>Veterinaria</th>
					<th>Direcci�n</th>
					<th>P�gina</th>
					<th>Logo</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="veterinaria" items="${veterinarias}">
					<tr>
						<c:if test="${val = 'ADMIN'}">
							<td><span title="Eliminar"><a
									href="${urlVet}/eliminar/${veterinaria.id}"><i
										class="fa fa-trash"></i></a></span></td>
						</c:if>
						<td><span title="Editar"><a
								href="${urlVet}/modificar/${veterinaria.id}"><i
									class="fa fa-pencil"></i></a></span>
						<td><span title="Editar"><a
								href="${urlGrilla}/calen/${veterinaria.id}">${veterinaria.nombre}</a></span></td>
						<td>${veterinaria.direccion}</td>
						<td>${veterinaria.pagina_web}</td>
						<td><img src="${urlPublic}/images/${veterinaria.imagen}"
							style="width: 75px; height: 75px;"></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>