<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>

<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/" var="baseURL" />

<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet"
	id="bootstrap-css">
<script src="${urlPublic}/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="${urlPublic}/css/simple-sidebar.css" rel="stylesheet">
<!-- Bootstrap core JavaScript -->
<script src="${urlPublic}/vendor/jquery/jquery.min.js"></script>
<script src="${urlPublic}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<jsp:include page="../includes/navbarHome.jsp" />
		
<div class="d-flex" id="wrapper">
	
	<!-- Sidebar -->
	<div class="bg-light border-right" id="sidebar-wrapper">
		<div class="list-group list-group-flush">
			<c:set var="rol" value="${user.rol.descripcion}"/>
			<c:choose>
				<c:when test="${rol == 'DUENO'}">
					<a href="${baseURL}vet/" class="list-group-item list-group-item-action bg-light">Tiendas Veterinarias</a>
					<a href="${baseURL}vete/" class="list-group-item list-group-item-action bg-light">M�dicos Veterinarios</a> 
				</c:when>
				<c:when test="${rol == 'VETE'}">
					<a href="${baseURL}vet/" class="list-group-item list-group-item-action bg-light">Tiendas Veterinarias</a>
					<a href="${baseURL}vete/" class="list-group-item list-group-item-action bg-light">M�dicos Veterinarios</a>
					<a href="${baseURL}duenos/" class="list-group-item list-group-item-action bg-light">Due�os de Mascotas</a>
					<a href="${baseURL}tipoTurno/" class="list-group-item list-group-item-action bg-light">Tipos de Turnos</a>
				</c:when>
				<c:when test="${rol == 'ADMIN'}">
					<a href="${baseURL}vet/" class="list-group-item list-group-item-action bg-light">Tiendas Veterinarias</a>
					<a href="${baseURL}vete/" class="list-group-item list-group-item-action bg-light">M�dicos Veterinarios</a>
					<a href="${baseURL}duenos/" class="list-group-item list-group-item-action bg-light">Due�os de Mascotas</a>
					<a href="${baseURL}tipoTurno/" class="list-group-item list-group-item-action bg-light">Tipos de Turnos</a>
					<a href="${baseURL}pers/" class="list-group-item list-group-item-action bg-light">Usuarios</a> 
				</c:when>
			</c:choose>
				<a href="${baseURL}map/" class="list-group-item list-group-item-action bg-light">Mapa</a> 
				
		</div>
	</div>
	<!-- /#sidebar-wrapper -->

	<!-- Page Content -->
	<div id="page-content-wrapper">
<link rel="stylesheet"
	href="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/css/ol.css">
	
	
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>Mapa de Tiendas Veterinarias</h2>
	</div>
	 <style>
      .map {
        width: 100%;
        height:400px;
      }
      #marker {
        width: 20px;
        height: 20px;
        border: 1px solid #088;
        border-radius: 10px;
        background-color: #0a0a0a;
        opacity: 0.5;
      }
      #marker1 {
        width: 20px;
        height: 20px;
        border: 1px solid #088;
        border-radius: 10px;
        background-color:#0a0a0a;
        opacity: 0.5;
      }
       #marker2 {
        width: 20px;
        height: 20px;
        border: 1px solid #088;
        border-radius: 10px;
        background-color: #0a0a0a;
        opacity: 0.5;
      }
       #marker3 {
        width: 20px;
        height: 20px;
        border: 1px solid #088;
        border-radius: 10px;
        background-color: #0a0a0a;
        opacity: 0.5;
      }
      #vienna {
        text-decoration: none;
        color: white;
        font-size: 11pt;
        font-weight: bold;
        text-shadow: black 0.1em 0.1em 0.2em;
      }
        #vienna1 {
        text-decoration: none;
        color: white;
        font-size: 11pt;
        font-weight: bold;
        text-shadow: black 0.1em 0.1em 0.2em;
      }
       #vienna2 {
        text-decoration: none;
        color: white;
        font-size: 11pt;
        font-weight: bold;
        text-shadow: black 0.1em 0.1em 0.2em;
      }
       #vienna3 {
        text-decoration: none;
        color: white;
        font-size: 11pt;
        font-weight: bold;
        text-shadow: black 0.1em 0.1em 0.2em;
      }
      .popover-content {
        min-width: 180px;
      }
    </style>
	<script
		src="https://cdn.rawgit.com/openlayers/openlayers.github.io/master/en/v5.3.0/build/ol.js"></script>
	<div id="map" class="map"></div>
	<div style="display: none;">
	<script src="https://cdn.jsdelivr.net/gh/openlayers/openlayers.github.io@master/en/v6.1.1/build/ol.js"></script>
	<div class="overlay" id="vienna" target="_blank"></div>
	<div class="overlay" id="vienna1" target="_blank"></div>
	<div class="overlay" id="vienna2" target="_blank"></div>
	<div class="overlay" id="vienna3" target="_blank"></div>
      <div id="marker" title="Marker"></div>
      <div id="marker1" title="Marker"></div>
      <div id="marker2" title="Marker"></div>
      <div id="marker3" title="Marker"></div>
      </div>
	<script>
		
		console.log(size);
		
			
			
		var map = new ol.Map({
			layers : [ new ol.layer.Tile({
				source : new ol.source.OSM()
			}) ],
			target : 'map',
			view : new ol.View({
				projection : 'EPSG:4326',
				center : [ -62.9966800, -40.8134500 ],
				zoom : 14
			})
		});
		var size = "${size}";
		if(size < 2){
				var idVet0 = "${idVeterinaria0}";
				var nameVet0 = "${nameVeterinaria0}";
				var latVet0 = "${latVeterinaria0}";
				var lonVet0 = "${longVeterinaria0}";

				var marker = new ol.Overlay({
					  position: [lonVet0,latVet0],
					  positioning: 'center-center',
					  element: document.getElementById('marker'),
					  stopEvent: false
					});
					map.addOverlay(marker);

					var content = document.getElementById('vienna');

					// Vienna label
					var vienna = new ol.Overlay({
					  position: [lonVet0,latVet0],
					  element: document.getElementById('vienna')
					});
					map.addOverlay(vienna);
					}
		else{
			if(size < 3){
				//primer marcador
				var idVet0 = "${idVeterinaria0}";
				var nameVet0 = "${nameVeterinaria0}";
				var latVet0 = "${latVeterinaria0}";
				var lonVet0 = "${longVeterinaria0}";

				var marker = new ol.Overlay({
					  position: [lonVet0,latVet0],
					  positioning: 'center-center',
					  element: document.getElementById('marker'),
					  stopEvent: false
					});
					map.addOverlay(marker);

					var content = document.getElementById('vienna');
					 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet0+"'>"+nameVet0+"</a>";

					// Vienna label
					var vienna = new ol.Overlay({
					  position: [lonVet0,latVet0],
					  element: document.getElementById('vienna')
					});
					map.addOverlay(vienna);

				//segundo marcador
				var idVet1 = "${idVeterinaria1}";
				var nameVet1 = "${nameVeterinaria1}";
				var latVet1 = "${latVeterinaria1}";
				var lonVet1 = "${longVeterinaria1}";

				
				var marker1 = new ol.Overlay({
				  position: [lonVet1,latVet1],
				  positioning: 'center-center',
				  element: document.getElementById('marker1'),
				  stopEvent: false
				});
				map.addOverlay(marker1);

				var content = document.getElementById('vienna1');
				 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet1+"'>"+nameVet1+"</a>";

				// Vienna label
				var vienna1 = new ol.Overlay({
				  position: [lonVet1,latVet1],
				  element: document.getElementById('vienna1')
				});
				map.addOverlay(vienna1);
				}
			else{
					if(size < 4){
						//primer marcador
						var idVet0 = "${idVeterinaria0}";
						var nameVet0 = "${nameVeterinaria0}";
						var latVet0 = "${latVeterinaria0}";
						var lonVet0 = "${longVeterinaria0}";

						var marker = new ol.Overlay({
							  position: [lonVet0,latVet0],
							  positioning: 'center-center',
							  element: document.getElementById('marker'),
							  stopEvent: false
							});
							map.addOverlay(marker);

							var content = document.getElementById('vienna');
							 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet0+"'>"+nameVet0+"</a>";

							// Vienna label
							var vienna = new ol.Overlay({
							  position: [lonVet0,latVet0],
							  element: document.getElementById('vienna')
							});
							map.addOverlay(vienna);

						//segundo marcador
						var idVet1 = "${idVeterinaria1}";
						var nameVet1 = "${nameVeterinaria1}";
						var latVet1 = "${latVeterinaria1}";
						var lonVet1 = "${longVeterinaria1}";

						
						var marker1 = new ol.Overlay({
						  position: [lonVet1,latVet1],
						  positioning: 'center-center',
						  element: document.getElementById('marker1'),
						  stopEvent: false
						});
						map.addOverlay(marker1);

						var content = document.getElementById('vienna1');
						 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet1+"'>"+nameVet1+"</a>";

						// Vienna label
						var vienna1 = new ol.Overlay({
						  position: [lonVet1,latVet1],
						  element: document.getElementById('vienna1')
						});
						map.addOverlay(vienna1);

						//tercer marcador
						var idVet2 = "${idVeterinaria2}";	
						var nameVet2 = "${nameVeterinaria2}";
						var latVet2 = "${latVeterinaria2}";
						var lonVet2 = "${longVeterinaria2}";

						var marker2 = new ol.Overlay({
							  position: [lonVet2,latVet2],
							  positioning: 'center-center',
							  element: document.getElementById('marker2'),
							  stopEvent: false
							});
							map.addOverlay(marker2);

							var content = document.getElementById('vienna2');
							 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet2+"'>"+nameVet2+"</a>";

							// Vienna label
							var vienna2 = new ol.Overlay({
							  position: [lonVet2,latVet2],
							  element: document.getElementById('vienna2')
							});
							map.addOverlay(vienna2);
						}
			else{
						if(size < 5){
							//primer marcador
							var idVet0 = "${idVeterinaria0}";
							var nameVet0 = "${nameVeterinaria0}";
							var latVet0 = "${latVeterinaria0}";
							var lonVet0 = "${longVeterinaria0}";

							var marker = new ol.Overlay({
								  position: [lonVet0,latVet0],
								  positioning: 'center-center',
								  element: document.getElementById('marker'),
								  stopEvent: false
								});
								map.addOverlay(marker);

								var content = document.getElementById('vienna');
								 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet0+"'>"+nameVet0+"</a>";

								// Vienna label
								var vienna = new ol.Overlay({
								  position: [lonVet0,latVet0],
								  element: document.getElementById('vienna')
								});
								map.addOverlay(vienna);

							//segundo marcador
							var idVet1 = "${idVeterinaria1}";
							var nameVet1 = "${nameVeterinaria1}";
							var latVet1 = "${latVeterinaria1}";
							var lonVet1 = "${longVeterinaria1}";

							
							var marker1 = new ol.Overlay({
							  position: [lonVet1,latVet1],
							  positioning: 'center-center',
							  element: document.getElementById('marker1'),
							  stopEvent: false
							});
							map.addOverlay(marker1);

							var content = document.getElementById('vienna1');
							 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet1+"'>"+nameVet1+"</a>";

							// Vienna label
							var vienna1 = new ol.Overlay({
							  position: [lonVet1,latVet1],
							  element: document.getElementById('vienna1')
							});
							map.addOverlay(vienna1);

							//tercer marcador
							var idVet2 = "${idVeterinaria2}";
							var nameVet2 = "${nameVeterinaria2}";
							var latVet2 = "${latVeterinaria2}";
							var lonVet2 = "${longVeterinaria2}";

							var marker2 = new ol.Overlay({
								  position: [lonVet2,latVet2],
								  positioning: 'center-center',
								  element: document.getElementById('marker2'),
								  stopEvent: false
								});
								map.addOverlay(marker2);

								var content = document.getElementById('vienna2');
								 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet2+"'>"+nameVet2+"</a>";

								// Vienna label
								var vienna2 = new ol.Overlay({
								  position: [lonVet2,latVet2],
								  element: document.getElementById('vienna2')
								});
								map.addOverlay(vienna2);

								//4to marcador
								var idVet3 = "${idVeterinaria3}";
							var nameVet3 = "${nameVeterinaria3}";
							var latVet3 = "${latVeterinaria3}";
							var lonVet3 = "${longVeterinaria3}";

							var marker3 = new ol.Overlay({
								  position: [lonVet3,latVet3],
								  positioning: 'center-center',
								  element: document.getElementById('marker3'),
								  stopEvent: false
								});
								map.addOverlay(marker3);

								var content = document.getElementById('vienna3');
								 content.innerHTML = "<a style='color:#000000;' href='http://localhost:8080/PetVet/grilla/calen/"+idVet3+"'>"+nameVet3+"</a>";

								// Vienna label
								var vienna3 = new ol.Overlay({
								  position: [lonVet3,latVet3],
								  element: document.getElementById('vienna3')
								});
								map.addOverlay(vienna3);

								console.log("veterinaria id = "+idVet2);
							}
						}
				}
		}
	</script>

		<!-- /#page-content-wrapper -->

		<!-- Menu Toggle Script -->	
		<script>
			$("#menu-toggle").click(function(e) {
				e.preventDefault();
				$("#wrapper").toggleClass("toggled");
			});
		</script>
