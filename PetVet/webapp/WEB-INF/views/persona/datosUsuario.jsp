<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="vi">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Alta Usuario</title>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="${urlForm}" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />

	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>${titulo}</h2>
		<p class="lead">${subtitulo}</p>
	</div>

	<div class="row" style="padding-left: 285px">
		<div class="col-md-8 order-md-1">

			<form:form class="needs-validation" action="${urlForm}/save" method="POST"
				modelAttribute="persona">
				<div class="row">
					<div class="col-md-6 mb-3">
						<label for="nombre">Nombre</label>
						<form:hidden path="id" value="${persona.id}" />
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token }" />
							<form:hidden path="rol" value="${persona.rol.id}" />
						<form:input type="text" class="form-control" id="nombre"
							path="nombre" placeholder="Nombre" value="${persona.nombre}"
							required="required" />
						<div class="invalid-feedback">Requiere ingresar nombre.</div>
					</div>
					<div class="col-md-6 mb-3">
						<label for="apellido">Apellido</label>
						<form:input type="text" class="form-control" id="apellido"
							path="apellido" placeholder="Apellido"
							value="${persona.apellido }" required="required" />
						<div class="invalid-feedback">Requiere ingresar apellido.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="username">Correo Electrónico</label>
					<div class="input-group">
						<form:input type="email" class="form-control" id="email"
							path="email" placeholder="Email" required="required"
							value="${persona.email}" />
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar username.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="password">Password</label>
					<div class="input-group">
						<form:input type="password" class="form-control" id="pass"
							path="pass" placeholder="Password" value="${persona.pass }"
							required="required" maxlength="8" minlength="8" />
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar password.</div>
					</div>
				</div>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block" type="submit"
					value="Guardar">Guardar</button>
			</form:form>
		</div>
	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>