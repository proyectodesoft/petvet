<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="${urlForm}" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<title>Due�os</title>
</head>
<body class="bg-light">

	<jsp:include page="../includes/menu.jsp" />
	
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
			<h2>${titulo}</h2>
			<p class="lead">${subtitulo}</p>
	</div>
	<c:set var="val" value="${user.rol.descripcion}"/>
	<c:if test="${val == 'ADMIN'}">
		<a href="${urlForm}/alta"><i class="fa fa-plus"></i></a>
	</c:if>
	<div class="table-responsive">
		<table class="table table-striped table-sm" id="myTable">
			<thead>
				<tr>
					<c:if test="${val == 'ADMIN'}">
						<th>#</th>
						<th>#</th>
					</c:if>
					<th>Nombre</th>
					<th>Apellido</th>
					<th>Correo Electr�nico</th>
					<th>Rol</th>
				</tr>
			</thead>
			<tbody>
			<tbody>
				<c:forEach var="usuario" items="${personas}">
					<tr>
						<c:if test="${val == 'ADMIN'}">
							<td><a href="${urlForm}/modificar/${usuario.id}"><i
									class="fa fa-edit"></i></a></td>
							<td><a href="${urlForm}/eliminar/${usuario.id}"><i
									class="fa fa-trash"></i></a></td>
						</c:if>
						<td>${usuario.nombre}</td>
						<td>${usuario.apellido}</td>
						<td>${usuario.email}</td>
						<td>${usuario.rol.descripcion}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
<jsp:include page="../includes/footer.jsp" />
</html>