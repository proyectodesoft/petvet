<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="vi">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Vista Usuario</title>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="${urlForm}" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />

	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h4>
				Usted es: <b>${duenoMascota.nombre} ${duenoMascota.apellido}</b>
			</h4>
			<small>Email: <b>${duenoMascota.email}</b>
			</small><br> <small>Tipo de Usuario: <b>${duenoMascota.tipo}</b>
			</small><br>
	</div>

	<div class="table-responsive">
		<table class="table table-striped table-sm" id="myTable">
			<thead>
				<tr>
					<th>Due�o de Mascota</th>
					<th>Veterinaria</th>
					<th>Fecha y Hora</th>
					<th>Duraci�n</th>
					<th>Tipo de Turno</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
			<tbody>
				<c:forEach var="turno" items="${turnos}">
					<tr>
						<td>${turno.duenoMascota.nombre} 
							${turno.duenoMascota.apellido}</td>
						<td>${turno.tipoTurno.descripcion}</td>
						<td>${turno.vete.nombre}</td>
						<td>${turno.fecha} - ${turno.hora}</td>
						<td>${turno.duracion} Minutos</td>
						<td>${turno.estado}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
<jsp:include page="../includes/footer.jsp" />
</html>