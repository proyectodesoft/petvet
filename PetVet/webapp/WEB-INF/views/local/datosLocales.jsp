<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<spring:url value="/resources" var="urlPublic" />
	<spring:url value="/local/save" var="urlForm" />
	<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

	</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>${titulo}</h2>
		<p class="lead">Aqui usted podr� administrar la Tienda de Mascotas</p>
	</div>

	<div class="row" style="padding-left: 285px">
		<div class="col-md-8 order-md-1">

			<form class="needs-validation" action="${urlForm}" method="post">
				<div class="row">
					<div class="col-md-6 mb-3">
						<label for="nombre">Nombre</label>
						<spring:bind path="local">
							<input type="hidden" class="form-control" name="id"
								required="required" value="${local.id}"/>
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token }" />
						</spring:bind>
						<spring:bind path="local">
							<input type="text" class="form-control" name="nombre"
								placeholder="Nombre" required="required" value="${local.nombre}"/>
						</spring:bind>
						<div class="invalid-feedback">Requiere ingresar un nombre.</div>
					</div>
					<div class="col-md-6 mb-3">
						<label for="cuit">Cuit</label>
						<spring:bind path="local">
							<input type="text" class="form-control" name="cuit"
								placeholder="Cuit" required="required" value="${local.cuit}"/>
						</spring:bind>
						<div class="invalid-feedback">Requiere ingresar un Cuit.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="pagina_web">Pagina Web</label>
					<div class="input-group">
						<spring:bind path="local">
							<input type="text" class="form-control" name="pagina_web"
								placeholder="Pagina Web" required="required" value="${local.pagina_web}"/>
						</spring:bind>
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar una Pagina Web.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="telefono">Tel�fono</label>
					<div class="input-group">
						<spring:bind path="local">
							<input type="text" class="form-control" name="telefono"
								placeholder="Tel�fono" required="required" value="${local.telefono}"/>
						</spring:bind>
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar un Tel�fono.</div>
					</div>
				</div>

				<div>
					<!-- div direcci�n -->

					<div class="mb-3">
						<label for="telefono">Ciudad</label>
						<div class="input-group">
							<spring:bind path="direccion">
								<input type="text" class="form-control" name="ciudad"
									placeholder="Ciudad" required="required" value="${direccion.ciudad}"/>
							</spring:bind>
							<div class="invalid-feedback" style="width: 100%;">
								Requiere ingresar una Ciudad.</div>
						</div>
					</div>

					<div class="mb-3">
						<label for="telefono">Barrio</label>
						<div class="input-group">
							<spring:bind path="direccion">
								<input type="text" class="form-control" name="barrio"
									placeholder="Barrio" required="required" value="${direccion.barrio}"/>
							</spring:bind>
							<div class="invalid-feedback" style="width: 100%;">
								Requiere ingresar un Barrio.</div>
						</div>
					</div>

					<div class="mb-3">
						<label for="telefono">Calle</label>
						<div class="input-group">
							<spring:bind path="direccion">
								<input type="text" class="form-control" name="calle"
									placeholder="Calle" required="required" value="${direccion.calle}"/>
							</spring:bind>
							<div class="invalid-feedback" style="width: 100%;">
								Requiere ingresar una Calle.</div>
						</div>
					</div>

					<div class="mb-3">
						<label for="telefono">N�mero</label>
						<div class="input-group">
							<spring:bind path="direccion">
								<input type="text" class="form-control" name="numero"
									placeholder="Ciudad" required="required" value="${direccion.numero}"/>
							</spring:bind>
							<div class="invalid-feedback" style="width: 100%;">
								Requiere ingresar un N�mero.</div>
						</div>
					</div>

					<div class="mb-3">
						<label for="telefono">Latitud</label>
						<div class="input-group">
							<spring:bind path="direccion">
								<input type="text" class="form-control" name="latitud"
									placeholder="Latitud" required="required" value="${direccion.latitud}"/>
							</spring:bind>
							<div class="invalid-feedback" style="width: 100%;">
								Requiere ingresar una Latitud.</div>
						</div>
					</div>

					<div class="mb-3">
						<label for="telefono">Longitud</label>
						<div class="input-group">
							<spring:bind path="direccion">
								<input type="text" class="form-control" name="longitud"
									placeholder="Longitud" required="required" value="${direccion.longitud}"/>
							</spring:bind>
							<div class="invalid-feedback" style="width: 100%;">
								Requiere ingresar una Longitud.</div>
						</div>
					</div>

				</div>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block" type="submit"
					value="Guardar">Guardar</button>
			</form>
		</div>

	</div>
</body>
	<jsp:include page="../includes/footer.jsp" />
</html>