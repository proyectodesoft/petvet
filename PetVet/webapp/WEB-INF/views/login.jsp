<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!doctype html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="">
<meta name="author"
	content="Mark Otto, Jacob Thornton, and Bootstrap contributors">
<meta name="generator" content="Jekyll v3.8.5">
<title>Registrate</title>

<link rel="canonical"
	href="https://getbootstrap.com/docs/4.3/examples/sign-in/">

<spring:url value="/resources" var="urlPublic" />
<spring:url value="/" var="urlRoot" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<!-- Bootstrap core CSS -->
<style>
.bd-placeholder-img {
	font-size: 1.125rem;
	text-anchor: middle;
	-webkit-user-select: none;
	-moz-user-select: none;
	-ms-user-select: none;
	user-select: none;
}

@media ( min-width : 768px) {
	.bd-placeholder-img-lg {
		font-size: 3.5rem;
	}
}
</style>
<!-- Custom styles for this template -->
<link href="${urlPublic}/css/signin.css" rel="stylesheet">
</head>
<body class="text-center">
	<div>
		<c:if test="${msg !=null }">
			<div class='alert alert-success mx-auto' role='alert'>${msg}</div>
		</c:if>
	</div>
	<form:form class="form-signin" action="${urlRoot}dologin" method="post" modelAttribute="userDTO">

		<img class="mb-4" src="${urlPublic}/images/PetVet.png" alt="" width="300px" height="150px">
		<h1 class="h3 mb-3 font-weight-normal">Login</h1>
		<label for="user" class="sr-only">Email</label> 
		<form:input type="email" id="email" path="email" class="form-control" placeholder="Email"/>
		<label for="password" class="sr-only">Contraseņa</label>
		<form:input type="password" id="password" path="password" class="form-control" placeholder="Contraseņa" />

		<button class="btn btn-lg btn-primary btn-block" type="submit">Guardar</button>
		
		<p class="mt-5 mb-3 text-muted">&copy; 2017-2019</p>
	</form:form>
</body>
</html>