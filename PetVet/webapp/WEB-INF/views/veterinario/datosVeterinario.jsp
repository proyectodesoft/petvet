<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/vete/save" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>Veterinario</title>
</head>
<body class="bg-light">

	<jsp:include page="../includes/menu.jsp" />
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>${titulo}</h2>
		<p class="lead">Aqui usted podr� adminsitrar veterinarios</p>
	</div>

	<div class="row" style="padding-left: 285px">
		<div class="col-md-8 order-md-1">

			<form:form class="needs-validation" action="${urlForm}" method="post"
				modelAttribute="veterinario">
				<div class="row">
					<div class="col-md-6 mb-3">
						<form:input type="hidden" class="form-control" path="id"
							value="${veterinario.id }" /> 
							<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token }" /> 
							<form:hidden path="rol.id" value="${veterinario.rol.id}"/>
							<label
							for="nombre">Nombre</label> <form:input type="text"
							class="form-control" path="nombre" id="nombre" placeholder="Nombre"
							required="required" value="${veterinario.nombre }" />
						<div class="invalid-feedback">Requiere ingresar un nombre.</div>
					</div>
					<div class="col-md-6 mb-3">
						<label for="cuit">Apellido</label> <form:input type="text"
							class="form-control" id="apellido" path="apellido" placeholder="Apellido"
							required="required" value="${veterinario.apellido}" />
						<div class="invalid-feedback">Requiere ingresar un Cuit.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="pagina_web">Matricula</label>
					<div class="input-group">
						<form:input type="text" class="form-control" id="matricula" path="matricula"
							placeholder="Matricula" required="required"
							value="${veterinario.matricula }" />
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar una matr�cula.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="pagina_web">Email</label>
					<div class="input-group">
						<form:input type="text" class="form-control" id="email" path="email"
							placeholder="Email" required="required"
							value="${veterinario.email }" />
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar un email.</div>
					</div>
				</div>

				<div class="mb-3">
					<label for="telefono">Password</label>
					<div class="input-group">
						<form:input type="password" class="form-control" id="pass" path="pass"
							placeholder="Password" required="required"
							value="${veterinario.pass }" />
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar una contrase�a.</div>
					</div>
				</div>

				<div class="col-md-6 mb-3">
					<label for="rol">Tienda Veterinaria</label> <br>
					<form:select path="vete.id">
						<form:options items="${tiendas}" itemValue="id" itemLabel="nombre" />
						<c:set var="val" value="${veternario.vete.id}" />
						<c:choose>
							<c:when test="${val == '0'}">
								<form:option selected="selected" value="0">Seleccione una Tienda </form:option>
							</c:when>
							<c:otherwise>
								<form:option selected="selected" value="${veternario.vete.id}" />
							</c:otherwise>
						</c:choose>
					</form:select>
				</div>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block" type="submit"
					value="Guardar">Guardar</button>
			</form:form>
		</div>

	</div>

</body>
<jsp:include page="../includes/footer.jsp" />
</html>