<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <spring:url value="/resources" var="urlPublic" />
    <spring:url value="/vete/modificar" var="urlEdit" />
    <spring:url value="/vete/alta" var="urlAlta" />
    <spring:url value="/vete/eliminar" var="urlEliminar" />
    <spring:url value="/vete/save" var="urlForm" />
    
	<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
    <title>Veterinarias</title>
</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />
  <div class="py-5 text-center">
    <img src="${urlPublic}/images/PetVet.png"	style="width: 350px; height: 150px;">
    <h2>Listado de M�dicos Veterinarios</h2>
    <p class="lead">Aqui usted podr� listar todas las veterinarias del sistema</p>
  </div>
  <c:set var="val" value="${user.rol.descripcion}"/>
  <c:set var="id_u" value="${user.id}" />

	<c:if test="${val == 'ADMIN'}">
  		<a href="${urlAlta}"><i class="fa fa-plus"></i></a>
  	</c:if>
  <div class="table-responsive">
        <table class="table table-striped table-sm" id="myTable">
          <thead>
            <tr>
            <c:if test="${val == 'ADMIN'}">
              <th>#</th>
              <th>#</th>
             </c:if>
              <th>Veterinario</th>
              <th>Matricua</th>
              <th>Usuario</th>
              <th>Tienda Veterinaria</th>
            </tr>
          </thead>
          <tbody>
          <c:forEach var="veterinario" items="${veterinarios}">
	          <tr>
	          	<c:if test="${val == 'ADMIN'}">
	          		<td><a href="${urlEdit}/${veterinario.id}"><i class="fa fa-edit"></i></a></td>
	          		<td><a href="${urlEliminar}/${veterinario.id}"><i class="fa fa-trash"></i></a></td>
	          	</c:if>
	          	<td>${veterinario.nombre}, ${veterinario.apellido}</td>
				<td>${veterinario.matricula}</td>
				<td>${veterinario.email}</td>
				<td>${veterinario.vete.nombre}</td>
	          </tr>
          </c:forEach>
          </tbody>
        </table>
      </div>
</body>
	<jsp:include page="../includes/footer.jsp" />
</html>