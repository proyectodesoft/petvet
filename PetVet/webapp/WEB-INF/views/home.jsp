<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/vet/modificar" var="urlEdit" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<title>PetVet - Home</title>

<style>
.product-device::before {
	position: absolute;
	top: 10%;
	right: 10px;
	bottom: 10%;
	left: 10px;
	content: "";
	background-color: rgba(255, 255, 255, .1);
	border-radius: 5px;
}

.product-device {
	position: absolute;
	right: 10%;
	bottom: -30%;
	width: 300px;
	height: 540px;
	background-color: #333;
	border-radius: 21px;
	-webkit-transform: rotate(30deg);
	transform: rotate(30deg);
}
</style>

</head>
<body class="bg-light">
	<jsp:include page="includes/menu.jsp" />
	<div
		class="position-relative overflow-hidden p-3 p-md-5 m-md-3 text-center bg-light">

		<div>
			<!-- Mensajes -->
			<c:if test="${msg !=null }">
				<div class='alert alert-success' role='alert'>${msg}</div>
			</c:if>
		</div>
		<!-- Mensajes -->
		
		<h2><c:out value="Bienvenid@ ${user.nombre} ${user.apellido}"/></h2>

		<div class="col-md-5 p-lg-5 mx-auto my-5">
			<h1 class="display-4 font-weight-normal">PetVet</h1>
			<p class="lead font-weight-normal">El mas potente software de
				seguimiento para Tiendas Veterinarias, Veterinarios y Due�os de
				Mascotas que les quieren brindar un cari�o tecnol�gico a sus
				mascotas</p>
		</div>
		<div class="product-device shadow-sm d-none d-md-block"></div>
		<div
			class="product-device product-device-2 shadow-sm d-none d-md-block">
			::before <img src="${urlPublic}/images/PetVet.png" width="280px"
				height="140px">
		</div>
	</div>
	<div class="d-md-flex flex-md-equal w-100 my-md-3 pl-md-3">
		<div
			class="bg-dark mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center text-white overflow-hidden">
			<div class="my-3 py-3">
				<h2 class="display-5">Seguimiento continuo</h2>
				<p class="lead">No te quedes sin conocer todos los detalles de
					la historia cl�nica de cada una de tus mascota.</p>
			</div>
			<div class="bg-light shadow-sm mx-auto"
				style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
				<img alt=""
					src="${urlPublic}/images/67084979-mascotas-expediente-m�dico-en-icono-del-portapapeles-ilustraci�n-de-dibujos-animados-de-la-historia-cl�nica.jpg"
					height="300px" width="340px">
			</div>
		</div>
		<div
			class="bg-light mr-md-3 pt-3 px-3 pt-md-5 px-md-5 text-center overflow-hidden">
			<div class="my-3 p-3">
				<h2 class="display-5">Horarios en tiempo real</h2>
				<p class="lead">Conoce, recib� alertas y mantene un constante
					minuto a minuto de los turnos de tus mascotas.</p>
			</div>
			<div class="bg-dark shadow-sm mx-auto"
				style="width: 80%; height: 300px; border-radius: 21px 21px 0 0;">
				<img alt="" src="${urlPublic}/images/300x0w.jpg">
			</div>
		</div>
	</div>
	<jsp:include page="includes/footer.jsp" />
</body>
</html>