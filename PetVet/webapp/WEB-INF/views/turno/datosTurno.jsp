<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/turno/save" var="urlForm" />
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
</head>

<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />

	<div class="py-5 text-center">
		<img src="${urlPublic}/images/logo.png"
			style="width: 250px; height: 250px;">
		<h2>${titulo}</h2>
		<p class="lead">Aqui usted podr� administrar los Turnos</p>
	</div>

	<div class="row" style="padding-left: 285px">
		<div class="col-md-8 order-md-1">

			<form:form class="needs-validation" action="${urlForm}" method="post"
				modelAttribute="turno">
				<!-- duenosMascota tipos turno -->
				<div class="mb-8">
					<form:input type="hidden" class="form-control" path="id" id="id"
						required="required" value="${turno.id}" />
						<form:input type="hidden" class="form-control" path="duracion" id="duracion"
						required="required" value="30" />
					<input type="hidden" name="${_csrf.parameterName}"
						value="${_csrf.token }" />
					<form:input type="hidden" class="form-control" path="duenoMascota.id" id="duenoMascota.id"
						required="required" value="${turno.duenoMascota.id}" />
						<form:input type="hidden" class="form-control" path="vete.id" id="vete.id"
						required="required" value="${turno.vete.id}" />
				</div>
				<div>
					<label>Fecha:</label>
					<form:input class="form-control" type="date" id="fecha"
						path="fecha" />
					<label>Hora:</label>
					<form:input class="form-control" type="time" id="hora" path="hora" />
				</div>
				<div class="mb-8">
					<label for="cuit">Estado</label>
					<form:input type="text" class="form-control" path="estado"
						id="estado" placeholder="Estado" value="${turno.estado}" />
				</div>
				<br>
				<div class=" mb-8">
					<label for="tipoTurno">Tipo de Turno</label>
					<div class="input-group">
						<form:select path="tipoTurno.id" id="tipoTurno.id">
							<c:forEach var="t" items="${tipos}">
								<form:option value="${t.id}">${t.nombre} - ${t.descripcion}</form:option>
							</c:forEach>
						</form:select>
					</div>
				</div>
				<br>
				<button class="btn btn-primary btn-lg btn-block" type="submit"
					value="Guardar">Guardar</button>
			</form:form>
		</div>
	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>