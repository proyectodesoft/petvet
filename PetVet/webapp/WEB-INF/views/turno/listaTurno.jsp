<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/turno" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<title>Turnos</title>
</head>
<body class="bg-light">

	<jsp:include page="../includes/menu.jsp" />

	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>Listado de Tipos de Turnos</h2>
		<p class="lead">Aqui usted podr� listar todos los Turnos del
			sistema</p>
	</div>
	<c:set var="val" value="${user.rol.descripcion}"/>
	<c:if test="${val == 'ADMIN'}">
		<a href="${urlAlta}"><i class="fa fa-plus"></i></a>
	</c:if>
	<div class="table-responsive">
		<table class="table table-striped table-sm" id="myTable">
			<thead>
				<tr>
					<c:if test="${val == 'ADMIN'}">
						<th>#</th>
						<th>#</th>
					</c:if>
					<th>Due�o de Mascota</th>
					<th>Veterinaria</th>
					<th>Fecha y Hora</th>
					<th>Duraci�n</th>
					<th>Tipo de Turno</th>
					<th>Estado</th>
				</tr>
			</thead>
			<tbody>
			<tbody>
				<c:forEach var="turno" items="${turnos}">
					<tr>
						<c:if test="${val == 'ADMIN'}">
							<td><a href="${urlForm}/modificar/${turno.id}"><i
									class="fa fa-edit"></i></a></td>
							<td><a href="${urlForm}/eliminar/${turno.id}"><i
									class="fa fa-trash"></i></a></td>
						</c:if>
						<td>${turno.duenoMascota.nombre}
							${turno.duenoMascota.apellido}</td>
						<td>${turno.tipoTurno.descripcion}</td>
						<td>${turno.vete.nombre}</td>
						<td>${turno.fecha} - ${turno.hora}</td>
						<td>${turno.duracion} Minutos</td>
						<td>${turno.estado}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
<jsp:include page="../includes/footer.jsp" />
</html>