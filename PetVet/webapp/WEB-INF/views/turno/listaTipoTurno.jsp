<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/tipoTurno/modificar" var="urlEdit" />
<spring:url value="/tipoTurno/alta" var="urlAlta" />
<spring:url value="/tipoTurno/eliminar" var="urlEliminar" />

<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<title>Tipos de Turnos</title>
</head>
<body class="bg-light">

	<jsp:include page="../includes/menu.jsp" />
	
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>Listado de Tipos de Turnos</h2>
		<p class="lead">Aqui usted podr� listar todos los Tipos de Turnos del sistema</p>
	</div>
	<c:set var="val" value="${user.rol.descripcion}"/>
	<c:if test="${val == 'ADMIN'}">
		<a href="${urlAlta}"><i class="fa fa-plus"></i></a>
	</c:if>
	<div class="table-responsive">
		<table class="table table-striped table-sm" id="myTable">
			<thead>
				<tr>
					<c:if test="${val == 'ADMIN'}">
						<th>#</th>
						<th>#</th>
					</c:if>
					<th>Nombre</th>
					<th>Descripci�n</th>
					<th>Especial</th>
				</tr>
			</thead>
			<tbody>
			<tbody>
				<c:forEach var="tipoTurno" items="${tipoTurnos}">
					<tr>
						<c:if test="${val == 'ADMIN'}">
							<td><a href="${urlEdit}/${tipoTurno.id}"><i
									class="fa fa-edit"></i></a></td>
							<td><a href="${urlEliminar}/${tipoTurno.id}"><i
									class="fa fa-trash"></i></a></td>
						</c:if>
						<td>${tipoTurno.nombre}</td>
						<td>${tipoTurno.descripcion}</td>
						<td>${tipoTurno.especial}</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>

</body>
<jsp:include page="../includes/footer.jsp" />
</html>