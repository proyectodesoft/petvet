<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="vi">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Alta Usuario</title>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/tipoTurno/save" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />

	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>Alta de Tipo de Turno</h2>
		<p class="lead">Aqui usted podr� dar administrar Tipos de Turnos</p>
	</div>

	<div class="row" style="padding-left: 285px">
		<div class="col-md-8 order-md-1">

			<form:form class="needs-validation" action="${urlForm}" method="POST"
				modelAttribute="tipoTurno">
				<div class="row">
					<div class="col-md-6 mb-3">
						<div class="mb-8">
							
						<form:hidden path="id" value="${tipoTurno.id}" />
						<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token }" />
						</div>
						<label for="nombre">Nombre</label>
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token }" />
						<form:input type="text" class="form-control" id="nombre"
							path="nombre" placeholder="Nombre" value="${tipoTurno.nombre}"
							required="required" />
						<div class="invalid-feedback">Requiere ingresar nombre.</div>
					</div>
					<div class="col-md-6 mb-3">
						<label for="apellido">Descripci�n</label>
						<form:input type="text" class="form-control" id="descripcion"
							path="descripcion" placeholder="Descripci�n"
							value="${tipoTurno.descripcion }" />
					</div>

					<div class="col mb-3">
						<p>
							<form:checkbox path="especial" id="especial"
								value="${tipoTurno.especial}" />
							Especial
						</p>
					</div>

					<button class="btn btn-primary btn-lg btn-block" type="submit"
						value="Guardar">Guardar</button>
			</form:form>
		</div>
	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>