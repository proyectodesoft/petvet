<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/fmt" prefix="fmt"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<link rel="stylesheet"
	href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/local" var="urlLocal" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">
<link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<title>Locales</title>

</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />
	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>Listado de Tiendas de Mascotas</h2>
		<p class="lead">Aqui usted podr� listar todas las Tiendas de Mascotas del
			sistema</p>
	</div>
	<div>
		<c:if test="${msg !=null }">
			<div class='alert alert-success' role='alert'>${msg}</div>
		</c:if>
	</div>
	<span title="Alta"><a href="${urlLocal}/alta"><i
			class="fa fa-plus"></i></a></span>
	<div class="table-responsive">
		<table class="table table-striped table-sm" id="myTable">
			<thead>
				<tr>
					<th>#</th>
					<th>#</th>
					<th>Local</th>
					<th>Direcci�n</th>
					<th>P�gina</th>
					<th>Logo</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="local" items="${locales}">
					<tr>
						<td><span title="Eliminar"><a
								href="${urlLocal}/eliminar/${local.id}"><i
									class="fa fa-trash"></i></a></span></td>
						<td><span title="Editar"><a
								href="${urlLocal}/modificar/${local.id}"><i
									class="fa fa-pencil"></i></a></span>
						<td>${local.nombre}</td>
						<td>${local.direccion}</td>
						<td>${local.telefono}</td>
						<td>${local.pagina_web}</td>
						<td>${local.cuit}</td>
						<td><img src="${urlPublic}/images/${local.imagen}"
							style="width: 75px; height: 75px;"></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>