<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/" var="baseURL" />

<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet"
	id="bootstrap-css">
<script src="${urlPublic}/js/bootstrap.min.js"></script>
<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
<link href="${urlPublic}/css/simple-sidebar.css" rel="stylesheet">
<!-- Bootstrap core JavaScript -->
<script src="${urlPublic}/vendor/jquery/jquery.min.js"></script>
<script src="${urlPublic}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<link rel="stylesheet"
	href="https://cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css">

	<jsp:include page="../includes/navbarHome.jsp" />
		
<div class="d-flex" id="wrapper">
	
	<!-- Sidebar -->
	<div class="bg-light border-right" id="sidebar-wrapper">
		<div class="list-group list-group-flush">
			<c:set var="rol" value="${user.rol.descripcion}"/>
			<c:choose>
				<c:when test="${rol == 'DUENO'}">
					<a href="${baseURL}vet/" class="list-group-item list-group-item-action bg-light">Tiendas Veterinarias</a>
					<a href="${baseURL}vete/" class="list-group-item list-group-item-action bg-light">M�dicos Veterinarios</a>
					<a href="${baseURL}mascotas/" class="list-group-item list-group-item-action bg-light">Mascotas</a>
				</c:when>
				<c:when test="${rol == 'VETE'}">
					<a href="${baseURL}vet/" class="list-group-item list-group-item-action bg-light">Tiendas Veterinarias</a>
					<a href="${baseURL}vete/" class="list-group-item list-group-item-action bg-light">M�dicos Veterinarios</a>
					<a href="${baseURL}duenos/" class="list-group-item list-group-item-action bg-light">Due�os de Mascotas</a>
					<a href="${baseURL}mascotas/" class="list-group-item list-group-item-action bg-light">Mascotas</a>
					<a href="${baseURL}tipoTurno/" class="list-group-item list-group-item-action bg-light">Tipos de Turnos</a>
				</c:when>
				<c:when test="${rol == 'ADMIN'}">
					<a href="${baseURL}vet/" class="list-group-item list-group-item-action bg-light">Tiendas Veterinarias</a>
					<a href="${baseURL}vete/" class="list-group-item list-group-item-action bg-light">M�dicos Veterinarios</a>
					<a href="${baseURL}duenos/" class="list-group-item list-group-item-action bg-light">Due�os de Mascotas</a>
					<a href="${baseURL}tipoTurno/" class="list-group-item list-group-item-action bg-light">Tipos de Turnos</a>
					<a href="${baseURL}mascotas/" class="list-group-item list-group-item-action bg-light">Mascotas</a>
					<a href="${baseURL}pers/" class="list-group-item list-group-item-action bg-light">Usuarios</a> 
				</c:when>
			</c:choose>
				<a href="${baseURL}map/" class="list-group-item list-group-item-action bg-light">Mapa</a>
		</div>
	</div>
	<!-- /#sidebar-wrapper -->

	<!-- Page Content -->
	<div id="page-content-wrapper">

		<!-- /#page-content-wrapper -->

		<!-- Menu Toggle Script -->	
		<script>
			$("#menu-toggle").click(function(e) {
				e.preventDefault();
				$("#wrapper").toggleClass("toggled");
			});
		</script>
