<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html lang="vi">
<head>
<meta charset="ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="ie=edge">
<title>Alta Usuario</title>
<spring:url value="/resources" var="urlPublic" />
<spring:url value="/mascotas/save" var="urlForm" />
<link href="${urlPublic}/css/bootstrap.min.css" rel="stylesheet">

<script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
</head>
<body class="bg-light">
	<jsp:include page="../includes/menu.jsp" />

	<div class="py-5 text-center">
		<img src="${urlPublic}/images/PetVet.png"
			style="width: 350px; height: 150px;">
		<h2>${titulo}</h2>
		<p class="lead">${subtitulo}</p>
	</div>

	<div class="row" style="padding-left: 285px">
		<div class="col-md-8 order-md-1">

			<form:form class="needs-validation" action="${urlForm}"
				method="POST" modelAttribute="mascota">
				<div class="row">
					<div class="col-md-6 mb-3">
						<label for="nombre">Nombre</label>
						<form:hidden path="id" id="id" value="${mascota.id}" />
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token }" />
						<form:input type="text" class="form-control" id="nombre"
							path="nombre" placeholder="Nombre" value="${mascota.nombre}"
							required="required" />
						<div class="invalid-feedback">Requiere ingresar nombre.</div>
					</div>
					<div class="col-md-6 mb-3">
						<label for="raza">Raza</label>
						<form:input type="text" class="form-control" id="raza" path="raza"
							placeholder="Raza" value="${mascota.raza}" required="required" />
						<div class="invalid-feedback">Requiere ingresar apellido.</div>
					</div>
				</div>

				<div class="mb-3">
					<form:select path="genero" id="genero" value="${mascota.genero}">
						<form:option value="m">Macho</form:option>
						<form:option value="f">Hembra</form:option>
					</form:select>
				</div>

				<div class="mb-3">
					<label for="fecha_nac">Fecha de nacimiento</label>
					<div class="input-group">
						<form:input type="date" class="form-control" id="fecha_nac"
							path="fecha_nac" placeholder="Fecha de Nacimiento"
							value="${mascota.fecha_nac }" required="required" maxlength="8"
							minlength="8" />
						<div class="invalid-feedback" style="width: 100%;">Requiere
							ingresar fecha de nacimiento.</div>
					</div>
				</div>

				<hr class="mb-4">
				<button class="btn btn-primary btn-lg btn-block" type="submit"
					value="Guardar">Guardar</button>
			</form:form>
		</div>
	</div>
</body>
<jsp:include page="../includes/footer.jsp" />
</html>