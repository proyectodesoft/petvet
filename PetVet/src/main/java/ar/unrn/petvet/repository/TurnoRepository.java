package ar.unrn.petvet.repository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.unrn.petvet.model.Turno;

public interface TurnoRepository extends JpaRepository<Turno, Integer>{
	
	@Query(value = "select t.id, t.duracion, t.id_dueno, t.estado, t.fecha, t.hora, t.id_vete, t.id_tipo "
			+ "from turnos t where t.id_vete like ?1", nativeQuery = true)
	List<Turno> findByVete(int idVete);
	
	@Query(value = "select t.id, t.duracion, t.id_dueno, t.estado, t.fecha, t.hora, t.id_vete, t.id_tipo "
			+ "from turnos t where t.id_dueno like ?1", nativeQuery = true)
	List<Turno> findByDuenoMascota(int idDuenoMascota);
	
	@Query(value = "select t.id, t.duracion, t.estado, t.fecha, t.hora, t.id_dueno, t.id_tipo, t.id_vete "
			+ "from turnos t "
			+ "where t.fecha = ?1\n" + 
			"and t.hora = ?2\n" + 
			"and t.id_vete = ?3\n" + 
			"and t.id_dueno = ?4", nativeQuery = true)
	Optional<Turno> findByFechaHoraVeteDuenoMascota(LocalDate fecha, LocalTime hora, int idVete, int idDueno);
}
