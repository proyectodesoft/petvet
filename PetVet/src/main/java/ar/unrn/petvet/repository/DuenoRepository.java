package ar.unrn.petvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unrn.petvet.model.DuenoMascota;

public interface DuenoRepository extends JpaRepository<DuenoMascota,Integer>{

}
