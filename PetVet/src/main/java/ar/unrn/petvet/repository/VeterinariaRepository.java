package ar.unrn.petvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unrn.petvet.model.TiendaVeterinaria;

public interface VeterinariaRepository extends JpaRepository<TiendaVeterinaria, Integer>{

}
