package ar.unrn.petvet.repository;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.unrn.petvet.model.Grilla;

public interface GrillaRepository extends JpaRepository<Grilla, Integer>{

	@Query(value = "select u.id, u.fecha, u.hora_ini, u.hora_fin, u.vete from grilla u where u.id_vete = ?1 and fecha = ?2", nativeQuery = true)
	List<Grilla> findByVeteFecha(int idVeterinaria, LocalDate fecha);
	
	@Query(value = "select u.id, u.descripcion, u.hora_ini, u.hora_fin, u.horario, u.id_vete from grilla u where u.id_vete = ?1", nativeQuery = true)
	Optional<Grilla> findByVete(int idVeterinaria);
	//List<Grilla> findByVete(int idVeterinaria);
}
