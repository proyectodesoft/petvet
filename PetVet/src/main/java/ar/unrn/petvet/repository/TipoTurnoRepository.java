package ar.unrn.petvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unrn.petvet.model.TipoTurno;

public interface TipoTurnoRepository extends JpaRepository<TipoTurno, Integer>{

}
