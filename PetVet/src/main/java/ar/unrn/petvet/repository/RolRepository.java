package ar.unrn.petvet.repository;


import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.unrn.petvet.model.Rol;

public interface RolRepository extends JpaRepository<Rol, Integer>{
	
	@Query(value = "select r.id, r.descripcion from roles r where r.descripcion like ?1", nativeQuery = true)
	Optional<Rol> findByDescripcion(String desc);
	
}
