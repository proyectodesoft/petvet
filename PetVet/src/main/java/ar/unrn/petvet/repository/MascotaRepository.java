package ar.unrn.petvet.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import ar.unrn.petvet.model.Mascota;

public interface MascotaRepository extends JpaRepository<Mascota, Integer>{
	
	@Query(value = "SELECT m.* FROM mascotas m WHERE m.id_dueno = ?1", nativeQuery = true)
	List<Mascota> findByDuenoMascota(int idDueno);
}
