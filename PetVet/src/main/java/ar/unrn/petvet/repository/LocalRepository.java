package ar.unrn.petvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unrn.petvet.model.Local;

public interface LocalRepository extends JpaRepository<Local,Integer>{

}
