package ar.unrn.petvet.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import ar.unrn.petvet.model.Veterinario;

public interface VeterinarioRepository extends JpaRepository<Veterinario, Integer>{

}
