package ar.unrn.petvet.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import ar.unrn.petvet.model.Persona;

@Repository
public interface PersonasRepository extends JpaRepository<Persona, Integer>{
	
	@Query(value = "select u.id, u.nombre, u.apellido, u.email, u.pass, u.matricula, u.tipo, u.id_rol from personas u where u.tipo like ?1", nativeQuery = true)
	List<Persona> findByTipo(String tipo);
	

//	@Query(value = "select u.id, u.nombre, u.apellido, u.email, u.pass, u.matricula, u.tipo from personas u where u.email like ?1", nativeQuery = true)
	Optional<Persona> findByEmail(String email);
	 
//	@Query(value = "select u.id, u.nombre, u.apellido, u.email, u.pass, u.matricula, u.tipo from personas u where u.email like ?1", nativeQuery = true)
//	Optional<UserDTO> findByUser(String emailAddress);
//	

}
