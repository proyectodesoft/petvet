package ar.unrn.petvet.model;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;
@Entity
@Table(name = "mascotas")
public class Mascota {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // auto_increment MySQL
	private int id;
	@NotNull
	private String nombre;
	@Column(columnDefinition = "date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate fecha_nac;
	@NotNull
	private String raza;
	@NotNull
	private String genero;
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_dueno", referencedColumnName = "id")
	private DuenoMascota duenoMascota;
	
	//CONSTRUCTOR
	public Mascota() {
		super();
	}
	
	public Mascota(String nombre, LocalDate fecha_nac, String raza, String genero, DuenoMascota dueno) {
		super();
		this.nombre = nombre;
		this.fecha_nac = fecha_nac;
		this.raza = raza;
		this.genero = genero;
		this.duenoMascota = dueno;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public LocalDate getFecha_nac() {
		return fecha_nac;
	}

	public void setFecha_nac(LocalDate fecha_nac) {
		this.fecha_nac = fecha_nac;
	}

	public String getRaza() {
		return raza;
	}

	public void setRaza(String raza) {
		this.raza = raza;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public DuenoMascota getDuenoMascota() {
		return duenoMascota;
	}

	public void setDuenoMascota(DuenoMascota duenoMascota) {
		this.duenoMascota = duenoMascota;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Mascota [id=");
		builder.append(id);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", fecha_nac=");
		builder.append(fecha_nac);
		builder.append(", raza=");
		builder.append(raza);
		builder.append(", genero=");
		builder.append(genero);
		builder.append(", duenoMascota=");
		builder.append(duenoMascota);
		builder.append("]");
		return builder.toString();
	}
	
	
}
