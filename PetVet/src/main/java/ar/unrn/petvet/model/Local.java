package ar.unrn.petvet.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "locales")
public class Local {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // auto_increment MySQL
	private int id;
	@NotNull
	private String nombre;
	@NotNull
	@Column(unique = true)
	private Long cuit; //clave �nica
	@NotNull
	private String telefono;
	@NotNull
	private String pagina_web;
	@OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "id_dire", referencedColumnName = "id")
	private Direccion direccion;
	@NotNull
	private String imagen = "default.jpg";
	
	//CONSTRUCTOR
	public Local() {
		super();
	}
	
	public Local(int id, String nombre, Long cuit, String telefono, String pagina_web, Direccion direccion,
			String imagen) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.cuit = cuit;
		this.telefono = telefono;
		this.pagina_web = pagina_web;
		this.direccion = direccion;
		this.imagen = imagen;
	}
	
	public Local(String nombre, Long cuit, String telefono, String pagina_web, Direccion direccion,
			String imagen) {
		super();
		this.nombre = nombre;
		this.cuit = cuit;
		this.telefono = telefono;
		this.pagina_web = pagina_web;
		this.direccion = direccion;
		this.imagen = imagen;
	}
	
	//GETTESR y SETTERS
	
	
	public String getNombre() {
		return nombre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Direccion getDireccion() {
		return direccion;
	}

	public void setDireccion(Direccion direccion) {
		this.direccion = direccion;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Long getCuit() {
		return cuit;
	}

	public void setCuit(Long cuit) {
		this.cuit = cuit;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getPagina_web() {
		return pagina_web;
	}

	public void setPagina_web(String pagina_web) {
		this.pagina_web = pagina_web;
	}

	public Direccion getDire() {
		return direccion;
	}

	public void setDire(Direccion dire) {
		this.direccion = dire;
	}

	public String getImagen() {
		return imagen;
	}

	public void setImagen(String imagen) {
		this.imagen = imagen;
	}
	//TO STRING
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Local [id=");
		builder.append(id);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", cuit=");
		builder.append(cuit);
		builder.append(", telefono=");
		builder.append(telefono);
		builder.append(", pagina_web=");
		builder.append(pagina_web);
		builder.append(", direccion=");
		builder.append(direccion);
		builder.append(", imagen=");
		builder.append(imagen);
		builder.append("]");
		return builder.toString();
	}



	
	
	
	

}
