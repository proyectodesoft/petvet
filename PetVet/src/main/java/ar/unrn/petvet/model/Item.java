package ar.unrn.petvet.model;

import java.time.LocalDateTime;
@SuppressWarnings("unused")
public class Item {
	
	private String descripcion;
	private LocalDateTime fecha_hora;
	private Veterinario veterinario;

	//CONSTRUCTOR
	public Item(String descripcion, LocalDateTime fecha_hora, Veterinario veterinario) {
		super();
		this.descripcion = descripcion;
		this.fecha_hora = fecha_hora;
		this.veterinario = veterinario;
	}

}
