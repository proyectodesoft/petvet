package ar.unrn.petvet.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "direcciones")
public class Direccion {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // auto_increment MySQL
	private int id;
	@NotNull
	private String calle;
	@NotNull
	private int numero;
	@NotNull
	private String barrio;
	@NotNull
	private String ciudad;
	@NotNull
	private Double latitud;
	@NotNull
	private Double longitud;
	
	@OneToOne(mappedBy = "direccion")
	private Local local;

	// CONSTRUCTOR
	public Direccion() {
		super();
	}
	
	public Direccion(int id, String calle, int numero, String barrio, String ciudad, Double latitud, Double longitud,
			Local local) {
		super();
		this.id = id;
		this.calle = calle;
		this.numero = numero;
		this.barrio = barrio;
		this.ciudad = ciudad;
		this.latitud = latitud;
		this.longitud = longitud;
		this.local = local;
	}



	//GETTERS y SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public String getBarrio() {
		return barrio;
	}

	public void setBarrio(String barrio) {
		this.barrio = barrio;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public Double getLatitud() {
		return latitud;
	}

	public void setLatitud(Double latitud) {
		this.latitud = latitud;
	}

	public Double getLongitud() {
		return longitud;
	}

	public void setLongitud(Double longitud) {
		this.longitud = longitud;
	}

	public Local getVete() {
		return local;
	}

	public void setVete(Local local) {
		this.local = local;
	}

	//TO STRING
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append(calle +" "+ numero + " - " + "Barrio " + barrio + " - " + ciudad);
		return builder.toString();
	}

}
