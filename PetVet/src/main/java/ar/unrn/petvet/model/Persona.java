package ar.unrn.petvet.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;

@Entity
@Table(name = "personas")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name="tipo")
public class Persona {

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // auto_increment MySQL
	private int id;
	@NotNull
	private String nombre;
	@NotNull
	private String apellido;
	@Email
	@NotNull
	@Column(unique = true)
	private String email;
	@NotNull
	private String pass;
	@ManyToOne
    @JoinColumn(name = "id_rol", referencedColumnName = "id")
	private Rol rol;
	@Column(name = "tipo", insertable = false, updatable = false)
	private String tipo;
	/**
     * Constructor sin parametros
     */
	public Persona() {
		super();
	}
	
	public Persona(String nombre, String apellido, String email, String pass) {
		super();
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.pass = pass;
	}
	
	public Persona(int id, String nombre, String apellido, String email, String pass, String tipo, Rol rol) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.apellido = apellido;
		this.email = email;
		this.pass = pass;
		this.tipo = tipo;
		this.rol = rol;
	}
	
	

	//GETTERS y SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public Rol getRol() {
		return rol;
	}

	public void setRol(Rol rol) {
		this.rol = rol;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Persona [id=");
		builder.append(id);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", apellido=");
		builder.append(apellido);
		builder.append(", email=");
		builder.append(email);
		builder.append(", pass=");
		builder.append(pass);
		builder.append(", tipo=");
		builder.append(tipo);
		builder.append(", rol=");
		builder.append(rol);
		builder.append("]");
		return builder.toString();
	}	

}
