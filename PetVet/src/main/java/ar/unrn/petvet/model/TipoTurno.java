package ar.unrn.petvet.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "tipoTurnos")
public class TipoTurno {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY) // auto_increment MySQL
	private int id;
	@NotNull
	private String nombre;
	private Boolean especial;
	private String descripcion;

	//CONSTRUCTORES
	public TipoTurno() {
		super();
	}
	
	public TipoTurno(String nombre, Boolean especial, String descripcion) {
		super();
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.especial = especial;
	}
	
	public TipoTurno(int id, String nombre, Boolean especial, String descripcion) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.especial = especial;
	}
	
//	public TipoTurno(String nombre, Boolean especial, String descripcion, List<Turno> turnos) {
//		super();
//		this.nombre = nombre;
//		this.descripcion = descripcion;
//		this.especial = especial;
//		this.turnos = turnos;
//	}
//	
//	public TipoTurno(int id, String nombre, Boolean especial, String descripcion, List<Turno> turnos) {
//		super();
//		this.id = id;
//		this.nombre = nombre;
//		this.descripcion = descripcion;
//		this.especial = especial;
//		this.turnos = turnos;
//	}

	//M�TODOS
	
//	public void addTurno(Turno turno) {
//		this.turnos.add(turno);
//	}
//	
//	public void removeTurno(Turno turno) {
//		this.turnos.remove(turno);
//	}
	
	//GETTERS y SETTERS
	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getEspecial() {
		return especial;
	}

	public void setEspecial(Boolean especial) {
		this.especial = especial;
	}

//	public List<Turno> getTurnos() {
//		return turnos;
//	}
//
//	public void setTurnos(List<Turno> turnos) {
//		this.turnos = turnos;
//	}

	//TO_STRING
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("TipoTurno [id=");
		builder.append(id);
		builder.append(", nombre=");
		builder.append(nombre);
		builder.append(", especial=");
		builder.append(especial);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append("]");
		return builder.toString();
	}
	

}
