package ar.unrn.petvet.model;

import java.util.ArrayList;
@SuppressWarnings("unused")
public class HistoriaClinica {
	
	private Mascota mascota;
	private ArrayList<Item> items;
	ArrayList<Persona> permisos;
	
	//CONSTRUCTOR
	public HistoriaClinica(Mascota mascota, ArrayList<Item> items, Persona permisos) {
		super();
		this.mascota = mascota;
		this.items = items;
		this.permisos = new ArrayList<Persona>();
	}
}
