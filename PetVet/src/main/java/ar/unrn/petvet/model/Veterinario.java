package ar.unrn.petvet.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
@DiscriminatorValue(value="veterinario")
public class Veterinario extends Persona{
	@NotNull
	@Column(unique = true)
	private String matricula;
	@ManyToOne
	@JoinColumn(name = "id_vete", referencedColumnName = "id")
	private TiendaVeterinaria vete;
	
	//CONSTRUCTOR
	
	public Veterinario() {
		
	}
	
	public Veterinario(int id, String nombre, String apellido, String user, String pass, String matricula, String tipo, Rol rol, TiendaVeterinaria vete) {
		super(id, nombre, apellido, user, pass, tipo, rol);
		this.matricula = matricula;
		this.vete = vete;
	}
	
	//GETTERS y SETTERS

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}
	
	public TiendaVeterinaria getVete() {
		return vete;
	}

	public void setVete(TiendaVeterinaria vete) {
		this.vete = vete;
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Veterinario [matricula=");
		builder.append(matricula);
		builder.append(", vete=");
		builder.append(vete);
		builder.append("]");
		return builder.toString();
	}

	//TO STRING
		

}
