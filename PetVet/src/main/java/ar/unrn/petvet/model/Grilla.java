package ar.unrn.petvet.model;

import java.sql.Time;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "grilla")
public class Grilla {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // auto_increment MySQL
	private int id;

	@NotNull
	private String descripcion;

	// temporalmente se guardara todo el html de la grilla aca.
	@NotNull
	private String horario;

	/*
	 * @NotNull private LocalDate fecha;
	 */
	@NotNull
	@ManyToOne
	@JoinColumn(name = "id_vete", referencedColumnName = "id")
	private TiendaVeterinaria vete;
//	@DateTimeFormat(pattern = "HH:mm")
	@NotNull
	private Time hora_ini;
//	@DateTimeFormat(pattern = "HH:mm")
	@NotNull
	private Time hora_fin;

	// esto se agregara en el sprint siguiente.
	// private ArrayList<Turno> turnos= new ArrayList<Turno>();

	// CONSTRUCTOR
	public Grilla() {
		super();
	}

	public Grilla(Time hora_ini, Time hora_fin, TiendaVeterinaria veterinaria, String horario,
			String descripcion) {
		super();
		this.hora_ini = hora_ini;
		this.hora_fin = hora_fin;
		this.horario = horario;
		this.descripcion = descripcion;
		// this.turnos = turnos;
		this.vete = veterinaria;
	}

	// GETTERS y SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Time getHora_ini() {
		return hora_ini;
	}

	public void setHora_ini(Time hora_ini) {
		this.hora_ini = hora_ini;
	}

	public Time getHora_fin() {
		return hora_fin;
	}

	public void setHora_fin(Time hora_fin) {
		this.hora_fin = hora_fin;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public TiendaVeterinaria getVete() {
		return vete;
	}

	public void setVete(TiendaVeterinaria veterinaria) {
		this.vete = veterinaria;
	}

	@Override
	public String toString() {
		String ln = System.getProperty("line.separator");
		StringBuilder builder = new StringBuilder();
		builder.append("Grilla [id=");
		builder.append(id);
		builder.append(ln);
		builder.append(", descripcion=");
		builder.append(descripcion);
		builder.append(ln);
		builder.append(", horario=");
		builder.append(horario);
		builder.append(ln);
		builder.append(", vete=");
		builder.append(vete);
		builder.append(ln);
		builder.append(", hora_ini=");
		builder.append(hora_ini);
		builder.append(ln);
		builder.append(", hora_fin=");
		builder.append(hora_fin);
		builder.append("]");
		return builder.toString();
	}

}
