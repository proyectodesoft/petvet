package ar.unrn.petvet.model;

import java.time.LocalDate;
import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "turnos")
public class Turno {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY) // auto_increment MySQL
	private int id;
	@NotNull
	private String duracion;
	@NotNull // pendiente, en curso, finalizado, cancelado
	private String estado;
	@ManyToOne
	@JoinColumn(name = "id_tipo", referencedColumnName = "id")
	private TipoTurno tipoTurno;
	@ManyToOne
	@JoinColumn(name = "id_dueno", referencedColumnName = "id")
	private DuenoMascota duenoMascota;
	@ManyToOne
	@JoinColumn(name = "id_vete", referencedColumnName = "id")
	private TiendaVeterinaria vete;
	@Column(columnDefinition = "date")
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate fecha;
	@Column(columnDefinition = "time")
	@DateTimeFormat(pattern = "HH:mm")
	private LocalTime hora;

	// CONSTRUCTOR
	public Turno() {
		super();
	}

	public Turno(String duracion, TipoTurno tipoTurno, DuenoMascota duenoMascota) {
		super();
		this.duracion = duracion;
		this.tipoTurno = tipoTurno;
		this.duenoMascota = duenoMascota;
		this.estado = "pendiente";
	}

	public Turno(int id, String duracion, TipoTurno tipoTurno, DuenoMascota duenoMascota, TiendaVeterinaria vete,
			LocalDate fecha, LocalTime hora, String estado) {
		super();
		this.id = id;
		this.duracion = duracion;
		this.tipoTurno = tipoTurno;
		this.duenoMascota = duenoMascota;
		this.vete = vete;
		this.fecha = fecha;
		this.hora = hora;
		this.estado = estado;
	}

	// GETTERS y SETTERS

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDuracion() {
		return duracion;
	}

	public void setDuracion(String duracion) {
		this.duracion = duracion;
	}

	public TipoTurno getTipoTurno() {
		return tipoTurno;
	}

	public void setTipoTurno(TipoTurno tipoTurno) {
		this.tipoTurno = tipoTurno;
	}

	public DuenoMascota getDuenoMascota() {
		return duenoMascota;
	}

	public void setDuenoMascota(DuenoMascota duenoMascota) {
		this.duenoMascota = duenoMascota;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public TiendaVeterinaria getVete() {
		return vete;
	}

	public void setVete(TiendaVeterinaria vete) {
		this.vete = vete;
	}

	public LocalDate getFecha() {
		return fecha;
	}

	public void setFecha(LocalDate fecha) {
		this.fecha = fecha;
	}

	public LocalTime getHora() {
		return hora;
	}

	public void setHora(LocalTime hora) {
		this.hora = hora;
	}

	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Turno [id=");
		builder.append(id);
		builder.append(", duracion=");
		builder.append(duracion);
		builder.append(", estado=");
		builder.append(estado);
		builder.append(", tipoTurno=");
		builder.append(tipoTurno);
		builder.append(", duenoMascota=");
		builder.append(duenoMascota);
		builder.append(", vete=");
		builder.append(vete);
		builder.append(", fecha=");
		builder.append(fecha);
		builder.append(", hora=");
		builder.append(hora);
		builder.append("]");
		return builder.toString();
	}

}