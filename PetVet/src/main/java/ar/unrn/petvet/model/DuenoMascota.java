package ar.unrn.petvet.model;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

@Entity
@DiscriminatorValue(value="duenoMascota")
public class DuenoMascota extends Persona{
	
//	@OneToMany(mappedBy = "duenoMascota", cascade = CascadeType.ALL)
//	Set<Mascota> mascotas = new HashSet<Mascota>();

	public DuenoMascota() {
		super();
	}
	
	public DuenoMascota(int id, String nombre, String apellido, String user, String pass, String tipo, Rol rol) {
		super(id, nombre, apellido, user, pass, tipo, rol);
	}

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("DuenoMascota [getId()=");
		builder.append(getId());
		builder.append(", getNombre()=");
		builder.append(getNombre());
		builder.append(", getApellido()=");
		builder.append(getApellido());
		builder.append(", getEmail()=");
		builder.append(getEmail());
		builder.append(", getPass()=");
		builder.append(getPass());
		builder.append(", getTipo()=");
		builder.append(getTipo());
		builder.append(", getRol()=");
		builder.append(getRol());
		builder.append(", toString()=");
		builder.append(super.toString());
		builder.append(", getClass()=");
		builder.append(getClass());
		builder.append(", hashCode()=");
		builder.append(hashCode());
		builder.append("]");
		return builder.toString();
	}

	


}
