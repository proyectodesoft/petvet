package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.Veterinario;

public interface IVeterinarioService {

	void insertar(Veterinario veterinario); 
	List<Veterinario> buscarTodos();
	void eliminar(int idVeterinario);
	Veterinario buscarPorId(int idVeterinario);
}
