package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.TipoTurno;

public interface ITipoTurnoService {

	void insertar(TipoTurno tipoDeTurno); 
	List<TipoTurno> buscarTodos();
	void eliminar(int idTipoDeTurnourno);
	TipoTurno buscarPorId(int idTipoDeTurno);
}
