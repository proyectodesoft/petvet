package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Veterinario;
import ar.unrn.petvet.repository.VeterinarioRepository;
import ar.unrn.petvet.service.IVeterinarioService;
@Service
public class VeterinarioServiceJPA implements IVeterinarioService{

	@Autowired
	VeterinarioRepository repo;
	
	public void insertar(Veterinario veterinario) {
		repo.save(veterinario);
	}

	public List<Veterinario> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idVeterinario) {
		repo.deleteById(idVeterinario);
	}

	public Veterinario buscarPorId(int idVeterinario) {
		Optional<Veterinario> optional = repo.findById(idVeterinario);
		if (optional.isPresent())
			return optional.get();
		return null;
	}
}

