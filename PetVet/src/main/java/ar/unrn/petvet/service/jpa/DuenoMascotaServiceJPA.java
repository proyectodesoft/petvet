package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.DuenoMascota;
import ar.unrn.petvet.repository.DuenoRepository;
import ar.unrn.petvet.service.IDuenoService;

@Service
public class DuenoMascotaServiceJPA implements IDuenoService{
	
	@Autowired
	DuenoRepository repo;

	public void insertar(DuenoMascota duenoMascota) {
		repo.save(duenoMascota);
	}

	public List<DuenoMascota> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idVeterinario) {
		repo.deleteById(idVeterinario);
	}

	public DuenoMascota buscarPorId(int idDuenoMascota) {
		Optional<DuenoMascota> o = repo.findById(idDuenoMascota);
		if (o.isPresent())
			return o.get();
		return null;
	}
	
	

}
