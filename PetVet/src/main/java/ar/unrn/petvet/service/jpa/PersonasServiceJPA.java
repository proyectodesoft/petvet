package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.repository.PersonasRepository;
import ar.unrn.petvet.service.IPersonaService;
@Service
public class PersonasServiceJPA implements IPersonaService{
	
	@Autowired
	private PersonasRepository repo;

	public void insertar(Persona persona) {
		repo.save(persona);
		
	}

	public List<Persona> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idPersona) {
		repo.deleteById(idPersona);
	}

	public Persona buscarPorId(int idPersona) {
		Optional<Persona> optional = repo.findById(idPersona);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

	public List<Persona> buscarPorTipo(String tipo) {
		return repo.findByTipo(tipo);
	}

	@Override
	public String getStringService1() {
		return null;
	}

	public Persona buscarPorEmail(String email) {
		Optional<Persona> optional = repo.findByEmail(email);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

//	public UserDTO buscarPorUser(String email) {
//		Optional<UserDTO> optional = repo.findByUser(email);
//		if (optional.isPresent())
//			return optional.get();
//		return null;
//	}


}
