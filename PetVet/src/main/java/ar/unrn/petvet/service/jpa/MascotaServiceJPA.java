package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Mascota;
import ar.unrn.petvet.repository.MascotaRepository;
import ar.unrn.petvet.service.IMascotaService;

@Service
public class MascotaServiceJPA implements IMascotaService{

	@Autowired
	MascotaRepository repo;
	
	public void insertar(Mascota mascota) {
		repo.save(mascota);
	}

	public List<Mascota> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idMascota) {
		repo.deleteById(idMascota);
	}

	public Mascota buscarPorId(int idMascota) {
		Optional<Mascota> optional = repo.findById(idMascota);
		if (optional.isPresent())
			return optional.get();
		return (new Mascota());
	}

	public List<Mascota> buscarPorDuenoMascota(int idDuenoMascota) {
		return repo.findByDuenoMascota(idDuenoMascota);
	}

}
