package ar.unrn.petvet.service.jpa;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Grilla;
import ar.unrn.petvet.repository.GrillaRepository;
import ar.unrn.petvet.service.IGrillaService;

@Service
public class GrillaServiceJPA implements IGrillaService{
	
	@Autowired
	private GrillaRepository repo;
	
	public void insertar(Grilla grilla) {
		repo.save(grilla);		
	}

	public List<Grilla> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idGrilla) {
		repo.deleteById(idGrilla);		
	}

	public Grilla buscarPorId(int idGrilla) {
		Optional<Grilla> optional = repo.findById(idGrilla);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

	public List<Grilla> buscarPorVetePorFecha(int idVeterinaria, LocalDate fecha) {
		return repo.findByVeteFecha(idVeterinaria, fecha);
	}

	public Grilla buscarByVete(int idVeterinaria) {
		Optional<Grilla> optional = repo.findByVete(idVeterinaria);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

}
