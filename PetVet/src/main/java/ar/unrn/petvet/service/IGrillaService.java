package ar.unrn.petvet.service;

import java.time.LocalDate;
import java.util.List;

import ar.unrn.petvet.model.Grilla;

public interface IGrillaService {
	
	void insertar(Grilla grilla); 
	List<Grilla> buscarTodos();
	void eliminar(int idGrilla);
	Grilla buscarPorId(int idGrilla);
	List<Grilla> buscarPorVetePorFecha(int idVeterinaria, LocalDate fecha);
	Grilla buscarByVete(int idVeterinaria);
}
