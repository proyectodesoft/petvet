package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.DuenoMascota;

public interface IDuenoService {
	void insertar(DuenoMascota duenoMascota); 
	List<DuenoMascota> buscarTodos();
	void eliminar(int idDuenoMascota);
	DuenoMascota buscarPorId(int idDuenoMascota);
}
