package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.TiendaVeterinaria;
import ar.unrn.petvet.repository.VeterinariaRepository;
import ar.unrn.petvet.service.IVeterinariaService;

@Service
public class VeterinariaServiceJPA implements IVeterinariaService{

	@Autowired
	VeterinariaRepository repo;
	
	public void insertar(TiendaVeterinaria veterinaria) {
		repo.save(veterinaria);		
	}

	public List<TiendaVeterinaria> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idVeterinaria) {
		repo.deleteById(idVeterinaria);
	}

	public TiendaVeterinaria buscarPorId(int idVeterinaria) {
		Optional<TiendaVeterinaria> optional = repo.findById(idVeterinaria);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

}
