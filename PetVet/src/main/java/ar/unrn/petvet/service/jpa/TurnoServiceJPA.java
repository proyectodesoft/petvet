package ar.unrn.petvet.service.jpa;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Turno;
import ar.unrn.petvet.repository.TurnoRepository;
import ar.unrn.petvet.service.ITurnoService;
@Service
public class TurnoServiceJPA implements ITurnoService{
	
	@Autowired
	TurnoRepository repo;

	public void insertar(Turno turno) {
		repo.save(turno);
	}

	public List<Turno> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idTurno) {
		repo.deleteById(idTurno);
	}

	public Turno buscarPorId(int idTurno) {
		Optional<Turno> optional = repo.findById(idTurno);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

	@Override
	public List<Turno> buscarPorTipo(String tipo) {
		return null;
	}

	public List<Turno> buscarPorVete(int idVete) {
		return repo.findByVete(idVete);
	}

	public Turno buscarPorFechaHoraVeteDuenoMascota(LocalDate fecha, LocalTime hora, int idVete, int idDueno) {
		Optional<Turno> optional = repo.findByFechaHoraVeteDuenoMascota(fecha, hora, idVete, idDueno);
		if (optional.isPresent())
			return optional.get();
		return (new Turno());
	}

	public List<Turno> buscarPorDuenoMascota(int idDuenoMascota) {
		return repo.findByDuenoMascota(idDuenoMascota);
	}

}
