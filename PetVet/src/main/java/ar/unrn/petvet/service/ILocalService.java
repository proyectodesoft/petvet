package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.Local;

public interface ILocalService {
	
	void insertar(Local local); 
	List<Local> buscarTodos();
	void eliminar(int idLocal);
	Local buscarPorId(int idLocal);
}
