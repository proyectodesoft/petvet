package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.TiendaVeterinaria;

public interface IVeterinariaService {
	
	void insertar(TiendaVeterinaria veterinaria); 
	List<TiendaVeterinaria> buscarTodos();
	void eliminar(int idVeterinaria);
	TiendaVeterinaria buscarPorId(int idVeterinaria);

}
