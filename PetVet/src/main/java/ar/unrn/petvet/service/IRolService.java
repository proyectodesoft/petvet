package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.Rol;

public interface IRolService {
	
	void insertar(Rol rol); 
	List<Rol> buscarTodos();
	void eliminar(int idRol);
	Rol buscarPorId(int idRol);
	Rol buscarPorDescripcion(String desc);
}
