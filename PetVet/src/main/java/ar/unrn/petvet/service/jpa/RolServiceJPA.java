package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Rol;
import ar.unrn.petvet.repository.RolRepository;
import ar.unrn.petvet.service.IRolService;

@Service
public class RolServiceJPA implements IRolService{

	@Autowired
	private RolRepository repo;
	
	public void insertar(Rol rol) {
		repo.save(rol);
	}

	public List<Rol> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idRol) {
		repo.deleteById(idRol);
	}

	public Rol buscarPorId(int idRol) {
		Optional<Rol> optional = repo.findById(idRol);
		if (optional.isPresent())
		return optional.get();
	return null;
	}
	
	public Rol buscarPorDescripcion(String desc) {
		Optional<Rol> optional = repo.findByDescripcion(desc);
		if (optional.isPresent())
		return optional.get();
	return null;
	}

}
