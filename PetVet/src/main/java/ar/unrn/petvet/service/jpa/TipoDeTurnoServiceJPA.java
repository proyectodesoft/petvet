package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.TipoTurno;
import ar.unrn.petvet.repository.TipoTurnoRepository;
import ar.unrn.petvet.service.ITipoTurnoService;
@Service
public class TipoDeTurnoServiceJPA implements ITipoTurnoService{
	
	@Autowired
	TipoTurnoRepository repo;

	public void insertar(TipoTurno tipoTurno) {
		repo.save(tipoTurno);		
	}

	public List<TipoTurno> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idTipoDeTurno) {
		repo.deleteById(idTipoDeTurno);
	}

	public TipoTurno buscarPorId(int idTipoTurno) {
		Optional<TipoTurno> optional = repo.findById(idTipoTurno);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

}
