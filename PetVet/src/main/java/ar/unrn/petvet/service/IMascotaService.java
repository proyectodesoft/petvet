package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.Mascota;

public interface IMascotaService {
	
	void insertar(Mascota mascota); 
	List<Mascota> buscarTodos();
	void eliminar(int idMascota);
	Mascota buscarPorId(int idMascota);
	List<Mascota> buscarPorDuenoMascota(int idDuenoMascota);

}
