package ar.unrn.petvet.service;

import java.util.List;

import ar.unrn.petvet.model.Persona;

public interface IPersonaService {
	
	void insertar(Persona persona); 
	List<Persona> buscarTodos();
	void eliminar(int idPersona);
	Persona buscarPorId(int idPersona);
	Persona buscarPorEmail(String email);
	List<Persona> buscarPorTipo(String tipo);
	String getStringService1();

}
