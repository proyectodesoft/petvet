package ar.unrn.petvet.service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

import ar.unrn.petvet.model.Turno;

public interface ITurnoService {

	void insertar(Turno turno); 
	List<Turno> buscarTodos();
	void eliminar(int idTurno);
	Turno buscarPorId(int idTurno);
	List<Turno> buscarPorTipo(String tipo);
	List<Turno> buscarPorVete(int idVete);
	List<Turno> buscarPorDuenoMascota(int idDuenoMascota);
	Turno buscarPorFechaHoraVeteDuenoMascota(LocalDate fecha, LocalTime hora, int idVete, int idDueno);
}
