package ar.unrn.petvet.service.jpa;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ar.unrn.petvet.model.Local;
import ar.unrn.petvet.repository.LocalRepository;
import ar.unrn.petvet.service.ILocalService;
@Service
public class LocalServiceJPA implements ILocalService{

	@Autowired
	private LocalRepository repo;
	
	public void insertar(Local local) {
		repo.save(local);
	}

	public List<Local> buscarTodos() {
		return repo.findAll();
	}

	public void eliminar(int idLocal) {
		repo.deleteById(idLocal);
	}

	public Local buscarPorId(int idLocal) {
		Optional<Local> optional = repo.findById(idLocal);
		if (optional.isPresent())
			return optional.get();
		return null;
	}

}
