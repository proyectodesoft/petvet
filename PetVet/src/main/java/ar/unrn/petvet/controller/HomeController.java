package ar.unrn.petvet.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.model.UserDTO;
import ar.unrn.petvet.service.IPersonaService;

@Controller
@SessionAttributes("userDTO")
@RequestMapping("/")
public class HomeController {

	@Autowired
	private IPersonaService repo;

	/*
	 * Add user in model attribute
	 */
	@ModelAttribute("userDTO")
	public UserDTO setUpUserForm() {
		return new UserDTO();
	}

//	@GetMapping("/")
//	public String mostrarIndex(Model model, HttpServletRequest request) {
//		HttpSession session = request.getSession(false);
//		if (session == null) {
//			System.out.println("ir a login");
//			return "login";
//		}
//		Persona pers = (Persona) session.getAttribute("user");
//		System.out.println("ir a home " + pers.toString());
//		return "home";
//	}

	@GetMapping("home")
	public String home(Model mdoel, HttpServletRequest request) {
		return "home";
	}

	@GetMapping("/")
	public String index(Model mdoel, HttpServletRequest request) {
		return "home";
	}

	@GetMapping("login")
	public String mostrarLogin(Model model) {
		model.addAttribute("userDTO", new UserDTO());
		return "login";
	}

	@PostMapping("dologin")
	public String doLogin(@ModelAttribute("userDTO") UserDTO user, RedirectAttributes attributes, Model model,
			HttpServletRequest request) {
		// Implement your business logic
		Persona p = repo.buscarPorEmail(user.getEmail());
		HttpSession session = request.getSession(true);
		if (p == null) {
			attributes.addFlashAttribute("msg", "Error al intentar iniciar sesi�n");
			return "redirect:login";
		} else {
			if (user.getEmail().equals(p.getEmail()) && user.getPassword().equals(p.getPass())) {
				// Set user dummy data
				session.setAttribute("user", p);
			} else {
				attributes.addFlashAttribute("msg", "Error al intentar iniciar sesi�n");
				return "redirect:login";
			}
		}
		return "redirect:home";
	}
	
	@GetMapping("logout")
	public String logout(RedirectAttributes attributes, Model model, HttpServletRequest request) {
		//Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		HttpSession session = request.getSession(false);
		session.invalidate();
		return "login";
	}

	@GetMapping(value = "/register")
	public String showRegistrationForm(Model model) {
		UserDTO userDto = new UserDTO();
		model.addAttribute("user", userDto);
		return "register";
	}

	@PostMapping(value = "/register")
	public String registerUser(@ModelAttribute("user") UserDTO accountDto, // BindingResult result, WebRequest request,
																			// Errors erros,
			Model model) {
		Persona registered = new Persona();
//		if (!result.hasErrors()) {
		registered = createUserAccount(accountDto);
//		}
		if (registered == null) {
			model.addAttribute("msg", "Error al intentar registrar al usuario");
		} else {
			System.out.println("persController: " + registered.toString());
			model.addAttribute("msg", "usuario creado con �xito.");
		}
		return "home";
	}

	private Persona createUserAccount(UserDTO accountDto) {
		Persona registered = repo.buscarPorEmail(accountDto.getEmail());
		if (registered == null) {
			registered = new Persona(accountDto.getNombre(), accountDto.getApellido(), accountDto.getEmail(),
					accountDto.getPassword());
			repo.insertar(registered);
		} else
			System.out.println(registered.toString());
		return registered;
	}
}
