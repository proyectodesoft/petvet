package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.DuenoMascota;
import ar.unrn.petvet.model.Turno;
import ar.unrn.petvet.service.IDuenoService;
import ar.unrn.petvet.service.IRolService;
import ar.unrn.petvet.service.ITurnoService;

@Controller
@RequestMapping("/duenos")
public class DuenoController {
	
	@Autowired
	private IDuenoService repo;
	
	@Autowired
	private IRolService repoRol;
	
	@Autowired
	private ITurnoService repoT;

	private String urlForm = "/duenos";

	@GetMapping("/")
	public String mostrarIndex(Model model, HttpServletRequest request) {
		List<DuenoMascota> duenos = repo.buscarTodos();
		for (DuenoMascota d : duenos) {
			d.setRol(repoRol.buscarPorId(d.getRol().getId()));
		}
		model.addAttribute("titulo", "Due�os de Mascotas");
		model.addAttribute("subtitulo", "Aqui usted podr� administrar los Due�os de Mascotas");
		model.addAttribute("duenos", duenos);
		model.addAttribute("urlForm", urlForm);
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/listaDuenos";
	}

		@GetMapping("/ver/{id}")
		public String ver(@PathVariable("id") int idDueno, Model model, HttpServletRequest request) {
			model.addAttribute("titulo", "Vista de Due�os de Mascota");
			model.addAttribute("subtitulo", "Aqui usted podr� ver sus datos");//TODO usar el titulo
			DuenoMascota p = new DuenoMascota();
			p = repo.buscarPorId(idDueno);
			model.addAttribute("duenoMascota", p);
			model.addAttribute("urlForm", urlForm);
			
			List<Turno> turnos = repoT.buscarPorDuenoMascota(idDueno);
			model.addAttribute("turnos", turnos);
			
			//session
			HttpSession session = request.getSession(true);
			session.setAttribute("user", session.getAttribute("user"));
			return "persona/viewDueno";
		}

	@GetMapping("/alta")
	public String alta(Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Alta de Due�os de Mascota");
		model.addAttribute("subtitulo", "Aqui usted podr� administrar Due�os de Mascotas");// TODO usar el titulo
		DuenoMascota p = new DuenoMascota();
		p.setRol(repoRol.buscarPorDescripcion("DUENO"));
		model.addAttribute("duenoMascota", p);
		model.addAttribute("urlForm", urlForm);
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/datosDueno";
	}

	@GetMapping("modificar/{id}")
	public String modificar(@PathVariable("id") int idDueno, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Modificaci�n de Due�o de Mascota");
		model.addAttribute("subtitulo", "Aqui usted podr� modificar Due�o de Mascotas");
		DuenoMascota p = repo.buscarPorId(idDueno);
		model.addAttribute("duenoMascota", p);
		model.addAttribute("urlForm", urlForm);
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/datosDueno";
	}

	@PostMapping("/save")
	public String guardar(@ModelAttribute DuenoMascota duenoMascota, BindingResult result,
			RedirectAttributes attributes, HttpServletRequest request) {
		duenoMascota.setRol(repoRol.buscarPorId(duenoMascota.getRol().getId()));
		repo.insertar(duenoMascota);
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "redirect:/duenos/";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminar(@PathVariable("id") int idDueno, Model model, RedirectAttributes attributes, HttpServletRequest request) {
		repo.eliminar(idDueno);
		attributes.addFlashAttribute("msg", "El registro fue eliminado");
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "redirect:/duenos/";
	}

}
