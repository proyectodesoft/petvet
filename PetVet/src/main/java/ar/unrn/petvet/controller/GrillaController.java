package ar.unrn.petvet.controller;

import java.sql.Time;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.Grilla;
import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.model.TiendaVeterinaria;
import ar.unrn.petvet.model.Turno;
import ar.unrn.petvet.service.jpa.DuenoMascotaServiceJPA;
import ar.unrn.petvet.service.jpa.GrillaServiceJPA;
import ar.unrn.petvet.service.jpa.TipoDeTurnoServiceJPA;
import ar.unrn.petvet.service.jpa.TurnoServiceJPA;
import ar.unrn.petvet.service.jpa.VeterinariaServiceJPA;

@Controller
@RequestMapping("/grilla")
public class GrillaController {
	
	@Autowired
	GrillaServiceJPA repo;
	@Autowired
	VeterinariaServiceJPA repoVet;
	@Autowired
	TurnoServiceJPA repoTur;
	@Autowired
	DuenoMascotaServiceJPA repoD;
	@Autowired
	TipoDeTurnoServiceJPA repoTi;
	
	@GetMapping("/")
	public String principal(Model model) {
//		List<Grilla> lista = repo.buscarTodos();
//		model.addAttribute("lista", lista);
		Grilla grilla = new Grilla();
		model.addAttribute("grilla", grilla);
		return "veterinaria/grilla";
	}
	
	@GetMapping("alta/{idVete}")
	public String alta(Model model, @PathVariable("idVete") int idVeterinaria, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		TiendaVeterinaria vete = repoVet.buscarPorId(idVeterinaria);
		model.addAttribute("vete", vete);
		Grilla grilla = new Grilla();
		grilla.setVete(vete);
		model.addAttribute("grilla", grilla);
		return "veterinaria/grilla";
	}
	
	@PostMapping("/save")
	public String guardar(@ModelAttribute Grilla grilla, BindingResult result, RedirectAttributes attributes,
			HttpServletRequest request
			) {
		System.out.println("en grilla controller " + grilla.toString());
		grilla.setVete(repoVet.buscarPorId(grilla.getVete().getId()));
		System.out.println("en grilla controller " + grilla.toString());
		repo.insertar(grilla);
    	attributes.addFlashAttribute("msg", "La grilla fue guardada.");
    	String ret = "redirect:/grilla/"+grilla.getVete().getId();
		return ret;
	}
	
	@GetMapping("/calen/{id}")
	public String principal2(@PathVariable("id") int idVeterinaria, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		session.setAttribute("user", p);

		TiendaVeterinaria vete = repoVet.buscarPorId(idVeterinaria);
		Grilla grilla = repo.buscarByVete(idVeterinaria);
		if (grilla == null) {
			grilla = new Grilla(Time.valueOf("08:00:00"), Time.valueOf("20:00:00"), vete, "30 Minutos", vete.getNombre());
		}
		grilla.setVete(vete);
		model.addAttribute("veterinaria", vete);
		model.addAttribute("grilla", grilla);
		
		Turno turno = new Turno();
		model.addAttribute("turno",turno);
		//tabla - horarios
		LocalDate fecha = LocalDate.now();
		LocalTime inicio = LocalTime.of(8, 0);
		LocalTime fin = LocalTime.of(20, 0);
		int dif = (fin.getHour() - inicio.getHour())*2;
		List<LocalTime> horarios = new ArrayList<LocalTime>();
		List<Turno> turnos = new ArrayList<Turno>();
		for (int i = 0; i <= dif; i++) {
			horarios.add(inicio);
			Turno t = repoTur.buscarPorFechaHoraVeteDuenoMascota(fecha, inicio, vete.getId(), p.getId());
			if (t.getId() == 0)
				t.setHora(inicio);
			else {
				if (t.getVete() != null)
					t.setVete(repoVet.buscarPorId(t.getId()));
				if (t.getDuenoMascota() != null)
					t.setDuenoMascota(repoD.buscarPorId(t.getDuenoMascota().getId()));
				if (t.getTipoTurno() != null)
					t.setTipoTurno(repoTi.buscarPorId(t.getTipoTurno().getId()));
			}
			turnos.add(t);
			inicio = inicio.plusMinutes(30);
		}
		model.addAttribute("fecha", fecha);
		model.addAttribute("horarios", horarios);
		model.addAttribute("turnos", turnos);
		Turno turno2 = new Turno();
		turno2.setFecha(fecha);
		turno2.setDuenoMascota(repoD.buscarPorId(p.getId()));
		model.addAttribute("turno", turno2);
		
		return "veterinaria/datosGrilla";
	}

	/* Busca la grilla correspondiente a la veterinaria para un d�a espec�fico*/
	//incompleto
	@GetMapping("calen/{id}/{fecha}")
	public String principal(@PathVariable("id") int idVeterinaria, @PathVariable("fecha")  LocalDate fecha, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		session.setAttribute("user", p);

		TiendaVeterinaria vete = repoVet.buscarPorId(idVeterinaria);
		Grilla grilla = repo.buscarByVete(idVeterinaria);
		if (grilla == null) {
			grilla = new Grilla(Time.valueOf("08:00:00"), Time.valueOf("20:00:00"), vete, "30 Minutos", vete.getNombre());
		}
		grilla.setVete(vete);
		model.addAttribute("veterinaria", vete);
		model.addAttribute("grilla", grilla);
		
		//tabla - horarios
//		LocalDate fecha = LocalDate.of(2019, 11, 18); //LocalDate.now();
		LocalTime inicio = LocalTime.of(8, 0);
		LocalTime fin = LocalTime.of(20, 0);
		int dif = (fin.getHour() - inicio.getHour())*2;
		List<LocalTime> horarios = new ArrayList<LocalTime>();
		List<Turno> turnos = new ArrayList<Turno>();
		for (int i = 0; i <= dif; i++) {
			horarios.add(inicio);
			Turno t = repoTur.buscarPorFechaHoraVeteDuenoMascota(fecha, inicio, vete.getId(), p.getId());
			if (t.getId() == 0)
				t.setHora(inicio);
			else {
				if (t.getVete() != null)
					t.setVete(repoVet.buscarPorId(t.getId()));
				if (t.getDuenoMascota() != null)
					t.setDuenoMascota(repoD.buscarPorId(t.getDuenoMascota().getId()));
				if (t.getTipoTurno() != null)
					t.setTipoTurno(repoTi.buscarPorId(t.getTipoTurno().getId()));
			}
			turnos.add(t);
			inicio = inicio.plusMinutes(30);
		}
		model.addAttribute("fecha", fecha);
		model.addAttribute("horarios", horarios);
		model.addAttribute("turnos", turnos);
		
		return "veterinaria/datosGrilla";
	}

	/* Busca la grilla correspondiente a la veterinaria para un d�a espec�fico*/
	//incompleto
	@PostMapping("buscar/{id}")
	public String principal(@PathVariable("id") int idVeterinaria, Turno turno2, Model model, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		session.setAttribute("user", p);

		TiendaVeterinaria vete = repoVet.buscarPorId(idVeterinaria);
		Grilla grilla = repo.buscarByVete(idVeterinaria);
		if (grilla == null) {
			grilla = new Grilla(Time.valueOf("08:00:00"), Time.valueOf("20:00:00"), vete, "30 Minutos", vete.getNombre());
		}
		grilla.setVete(vete);
		model.addAttribute("veterinaria", vete);
		model.addAttribute("grilla", grilla);
		
		Turno turno = new Turno();
		model.addAttribute("turno",turno);
		//tabla - horarios
		LocalDate fecha = turno2.getFecha();
		LocalTime inicio = LocalTime.of(8, 0);
		LocalTime fin = LocalTime.of(20, 0);
		int dif = (fin.getHour() - inicio.getHour())*2;
		List<LocalTime> horarios = new ArrayList<LocalTime>();
		List<Turno> turnos = new ArrayList<Turno>();
		for (int i = 0; i <= dif; i++) {
			horarios.add(inicio);
			Turno t = repoTur.buscarPorFechaHoraVeteDuenoMascota(fecha, inicio, vete.getId(), p.getId());
			if (t.getId() == 0)
				t.setHora(inicio);
			else {
				if (t.getVete() != null)
					t.setVete(repoVet.buscarPorId(t.getId()));
				if (t.getDuenoMascota() != null)
					t.setDuenoMascota(repoD.buscarPorId(t.getDuenoMascota().getId()));
				if (t.getTipoTurno() != null)
					t.setTipoTurno(repoTi.buscarPorId(t.getTipoTurno().getId()));
			}
			turnos.add(t);
			inicio = inicio.plusMinutes(30);
		}
		model.addAttribute("fecha", fecha);
		model.addAttribute("horarios", horarios);
		model.addAttribute("turnos", turnos);
		
		return "veterinaria/datosGrilla";
	}

}
