package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.TipoTurno;
import ar.unrn.petvet.model.Turno;
import ar.unrn.petvet.service.jpa.TipoDeTurnoServiceJPA;

@Controller
@RequestMapping("/tipoTurno")
public class TipoTurnoController {
	
	@Autowired
	private TipoDeTurnoServiceJPA repo;
	
	@GetMapping("/")
	public String mostrarIndex(Model model) {
		List<TipoTurno> tipoTurnos = repo.buscarTodos();
		model.addAttribute("tipoTurnos", tipoTurnos);
		return "turno/listaTipoTurno";
	}
	
	@GetMapping("/alta")
	public String alta(Model model, @ModelAttribute TipoTurno tipoTurno) {
		model.addAttribute("titulo", "Alta de turno");
		Turno t = new Turno();
		model.addAttribute("turno", t);
		//falta mandar la lista de tipos de turnos
		return "turno/datosTipoTurno";
	}
	
	@GetMapping("/modificar/{id}")
	public String modificar(@PathVariable("id") int idTipoTurno, Model model) {
		model.addAttribute("titulo", "Modificar el Tipo de Turno");
		TipoTurno t = repo.buscarPorId(idTipoTurno);
		model.addAttribute("tipoTurno", t);
		return "turno/datosTipoTurno";
	}
	
	@PostMapping("/save")
	public String guardar(@ModelAttribute TipoTurno tipoTurno, BindingResult result, RedirectAttributes attributes,
			HttpServletRequest request
			) {
		repo.insertar(tipoTurno);
    	attributes.addFlashAttribute("msg", "El registro fue guardado");		
		return "redirect:/tipoTurno/";
	}
}
