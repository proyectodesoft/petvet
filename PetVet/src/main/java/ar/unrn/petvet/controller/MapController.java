package ar.unrn.petvet.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import ar.unrn.petvet.model.TiendaVeterinaria;
import ar.unrn.petvet.service.jpa.VeterinariaServiceJPA;

@Controller
@RequestMapping("/map")
public class MapController {

	@Autowired
	private VeterinariaServiceJPA repo;	

	@GetMapping("/")
	public String mostrarIndex(Model model) {
		List<TiendaVeterinaria> listVeterinaria = repo.buscarTodos();
		int listSize = 0;
		for(int i = 0; i< listVeterinaria.size(); i++) {
			model.addAttribute("idVeterinaria"+i, listVeterinaria.get(i).getId());
			model.addAttribute("nameVeterinaria"+i, listVeterinaria.get(i).getNombre());
			model.addAttribute("latVeterinaria"+i, listVeterinaria.get(i).getDireccion().getLatitud());
			model.addAttribute("longVeterinaria"+i, listVeterinaria.get(i).getDireccion().getLongitud());
			listSize++;
		}
		model.addAttribute("size", listSize);
		
		return "mapa/mapMarker";
	}
}
