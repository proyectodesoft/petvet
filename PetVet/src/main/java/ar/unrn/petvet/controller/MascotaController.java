package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.Mascota;
import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.service.IDuenoService;
import ar.unrn.petvet.service.IMascotaService;

@Controller
@RequestMapping("/mascotas")
public class MascotaController {
	
	@Autowired
	private IMascotaService repo;
	@Autowired
	private IDuenoService repoD;

	private String urlForm = "/mascotas";

	@GetMapping("/")
	public String mostrarIndex(Model model, HttpServletRequest request) {
		List<Mascota> mascotas = repo.buscarTodos();
		for (Mascota m : mascotas) {
			m.setDuenoMascota(repoD.buscarPorId(m.getDuenoMascota().getId()));
			System.out.println(m);
		}
		model.addAttribute("titulo", "Mascotas");
		model.addAttribute("subtitulo", "Aqui usted podr� administrar las Mascotas");
		model.addAttribute("mascotas", mascotas);
		model.addAttribute("urlForm", urlForm);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "mascotas/listaMascotas";
	}

	@GetMapping("/alta")
	public String alta(Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Alta de Mascota");
		model.addAttribute("subtitulo", "Aqui usted podr� administrar Mascotas");// TODO usar el titulo
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		
		session.setAttribute("user", p);
		
		Mascota m = new Mascota();
		m.setDuenoMascota(repoD.buscarPorId(p.getId()));
		model.addAttribute("mascota", m);
		model.addAttribute("urlForm", urlForm);
		return "mascotas/datosMascota";
	}

	@GetMapping("modificar/{id}")
	public String modificar(@PathVariable("id") int idMascota, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Modificaci�n de la Mascota");
		model.addAttribute("subtitulo", "Aqui usted podr� modificar  Mascotas");
		
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		
		session.setAttribute("user", p);
		
		Mascota m = new Mascota();
		m = repo.buscarPorId(idMascota);
		m.setDuenoMascota(repoD.buscarPorId(p.getId()));
		model.addAttribute("mascota", m);
		model.addAttribute("urlForm", urlForm);
		return "mascotas/datosMascota";
	}

	@PostMapping("/save")
	public String guardar(@ModelAttribute Mascota mascota, BindingResult result,
			RedirectAttributes attributes, HttpServletRequest request) {
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		
		mascota.setDuenoMascota(repoD.buscarPorId(p.getId()));
		repo.insertar(mascota);
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		return "redirect:/mascotas/";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminar(@ModelAttribute("mascota") Mascota mascota, Model model, BindingResult result,
			RedirectAttributes attributes) {
		repo.eliminar(mascota.getId());
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		return "redirect:/mascotas/";
	}
}
