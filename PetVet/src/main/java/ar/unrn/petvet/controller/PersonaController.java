package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.model.Rol;
import ar.unrn.petvet.service.IPersonaService;
import ar.unrn.petvet.service.IRolService;

@Controller
@RequestMapping("/pers")
public class PersonaController {

	@Autowired
	private IPersonaService repo;
	@Autowired
	private IRolService repoRol;

	private String urlForm = "/pers";

	@GetMapping("/")
	public String mostrarIndex(Model model, HttpServletRequest request) {
		List<Persona> personas = repo.buscarPorTipo("Persona");
		for (Persona p : personas) {
			p.setRol(repoRol.buscarPorId(p.getRol().getId()));
		}
		model.addAttribute("titulo", "Usuarios");
		model.addAttribute("subtitulo", "Aqui usted podr� administrar usuarios");
		model.addAttribute("personas", personas);
		model.addAttribute("urlForm", urlForm);
		// session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/listaUsuario";
	}

	@GetMapping("/alta")
	public String alta(Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Alta de usuarios");
		model.addAttribute("subtitulo", "Aqui usted podr� dar de alta usuarios");
		Persona p = new Persona();
		model.addAttribute("persona", p);
		p.setRol(repoRol.buscarPorDescripcion("ADMIN"));
		model.addAttribute("urlForm", urlForm);
		// session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/datosUsuario";
	}

	@GetMapping("modificar/{id}")
	public String modificar(@PathVariable("id") int idPersona, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Modificaci�n de usuarios");
		model.addAttribute("subtitulo", "Aqui usted podr� modificar un usuario");
		Persona p = repo.buscarPorId(idPersona);
		model.addAttribute("persona", p);
		List<Rol> roles = repoRol.buscarTodos();
		model.addAttribute("roles", roles);
		model.addAttribute("urlForm", urlForm);
		// session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/datosUsuario";
	}

	@PostMapping("/save")
	public String guardar(@ModelAttribute Persona persona, BindingResult result, RedirectAttributes attributes,
			HttpServletRequest request) {
		persona.setRol(repoRol.buscarPorId(persona.getRol().getId()));
		repo.insertar(persona);
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		// session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "redirect:/pers/";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminar(@PathVariable("id") int idPersona, Model model, RedirectAttributes attributes,
			HttpServletRequest request) {
		repo.eliminar(idPersona);
		attributes.addFlashAttribute("msg", "El registro fue guardado");

		// session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "redirect:/pers/";
	}

	@GetMapping("/ver/{id}")
	public String ver(@PathVariable("id") int idPersona, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Vista de Persona");
		model.addAttribute("subtitulo", "Aqui usted podr� ver sus datos");// TODO usar el titulo
		Persona p = new Persona();
		p = repo.buscarPorId(idPersona);
		System.out.println("persona controller: " + p.toString());
		model.addAttribute("persona", p);
		model.addAttribute("urlForm", urlForm);

		// session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/viewPersona";
	}

}
