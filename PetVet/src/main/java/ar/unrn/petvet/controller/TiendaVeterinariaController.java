package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.Direccion;
import ar.unrn.petvet.model.TiendaVeterinaria;
import ar.unrn.petvet.service.jpa.VeterinariaServiceJPA;

@Controller
@RequestMapping("/vet")
public class TiendaVeterinariaController {

	@Autowired
	VeterinariaServiceJPA repo;
	
	//get lista de veterinaria
    
    @RequestMapping(value = "/api/veterinarias/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<TiendaVeterinaria>> findAll() {
   // 	List<TiendaVeterinaria> lista = repo.findAllVeterinaria();
        return new ResponseEntity<List<TiendaVeterinaria>>(/*lista */HttpStatus.OK);
        
    }
	

	@GetMapping("/")
	public String principal(Model model, HttpServletRequest request) {
		List<TiendaVeterinaria> lista = repo.buscarTodos();
		model.addAttribute("veterinarias", lista);
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "veterinaria/listaVeterinarias";
	}

	@GetMapping("/alta")
	public String alta(ModelMap model) {
		model.addAttribute("titulo", "Alta de Veterinaria");
		TiendaVeterinaria v = new TiendaVeterinaria();
		Direccion d = new Direccion();
		model.addAttribute("veterinaria", v);
		model.addAttribute("direccion", d);
		return "veterinaria/datosVeterinaria";
	}

	@GetMapping("/modificar/{id}")
	public String modifcar(@PathVariable("id") int idVeterinaria, Model model) {
		model.addAttribute("titulo", "Modificar la Veterinaria");
		TiendaVeterinaria v = repo.buscarPorId(idVeterinaria);
		model.addAttribute("veterinaria", v);
		model.addAttribute("direccion", v.getDireccion());
		return "veterinaria/datosVeterinaria";
	}

	@GetMapping("/eliminar/{id}")
	public String eliminar(@ModelAttribute("veterinaria") TiendaVeterinaria veterinaria, @ModelAttribute("direccion") Direccion direccion, Model model, BindingResult result,
			RedirectAttributes attributes) {
		repo.eliminar(veterinaria.getId());
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		return "redirect:/vet/";
	}
	
	@PostMapping("/save")
	public String guardar(@ModelAttribute("veterinaria") TiendaVeterinaria veterinaria, @ModelAttribute("direccion") Direccion direccion,
			Model model, BindingResult result, RedirectAttributes attributes) {
		veterinaria.setDireccion(direccion);
		repo.insertar(veterinaria);
		attributes.addFlashAttribute("msg", "El registro fue guardado con �xito.");
		return "redirect:/vet/";
	}

}
