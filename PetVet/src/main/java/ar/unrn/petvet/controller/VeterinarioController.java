package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.DuenoMascota;
import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.model.TiendaVeterinaria;
import ar.unrn.petvet.model.Veterinario;
import ar.unrn.petvet.service.IRolService;
import ar.unrn.petvet.service.IVeterinariaService;
import ar.unrn.petvet.service.IVeterinarioService;

@Controller
@RequestMapping("/vete")
public class VeterinarioController {

	@Autowired
	private IVeterinarioService repo;
	@Autowired
	private IVeterinariaService repot;
	@Autowired
	private IRolService repoRol;
	
	@GetMapping("/")
	public String mostrarIndex(Model model, HttpServletRequest request) {
		List<Veterinario> veterinarios = repo.buscarTodos();
		model.addAttribute("veterinarios", veterinarios);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "veterinario/listaVeterinarios";
	}
	
	@GetMapping("/alta")
	public String alta(Model model, @ModelAttribute Veterinario veterinario, HttpServletRequest request) {
		model.addAttribute("titulo", "Alta de veterinario");
		Veterinario v = new Veterinario();
		List<TiendaVeterinaria> veterinarias = repot.buscarTodos();
		v.setRol(repoRol.buscarPorDescripcion("VETE"));
		model.addAttribute("veterinario", v);
		model.addAttribute("tiendas", veterinarias);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "veterinario/datosVeterinario";
	}
	
	@GetMapping("/modificar/{id}")
	public String modificar(@PathVariable("id") int idVeterinario, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Modificar al veterinario");
		Veterinario v = repo.buscarPorId(idVeterinario);
		List<TiendaVeterinaria> veterinarias = repot.buscarTodos();
		model.addAttribute("veterinario", v);
		model.addAttribute("tiendas", veterinarias);
		
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "veterinario/datosVeterinario";
	}
	
	@PostMapping("/save")
	public String guardar(@ModelAttribute Veterinario veterinario, BindingResult result, RedirectAttributes attributes,
			HttpServletRequest request
			) {
		repo.insertar(veterinario);
		attributes.addFlashAttribute("msg", "El registro fue guardado");	

		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "redirect:/vete/";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminar(@PathVariable("id") int idVeterinario, Model model, RedirectAttributes attributes, HttpServletRequest request) {
		repo.eliminar(idVeterinario);
		attributes.addFlashAttribute("msg", "El registro fue eliminado");		
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
	return "redirect:/vete/";
	}
	
	@GetMapping("/ver/{id}")
	public String ver(@PathVariable("id") int idVeterinario, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Vista de Veterinario");
		model.addAttribute("subtitulo", "Aqui usted podr� ver sus datos");//TODO usar el titulo
		Persona p = repo.buscarPorId(idVeterinario);
		System.out.println(p);
		model.addAttribute("veterinario", p);
		
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "persona/viewDueno";
	}
}

