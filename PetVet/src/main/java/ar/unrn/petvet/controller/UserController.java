package ar.unrn.petvet.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttribute;

import ar.unrn.petvet.model.UserDTO;

@Controller
@RequestMapping("/user")
public class UserController {

	 /*
	    * Get user from session attribute
	    */
	   @GetMapping("/info")
	   public String userInfo(@SessionAttribute("userDTO") UserDTO user) {

	      System.out.println("Email: " + user.getEmail());
	      System.out.println("Name: " + user.getNombre() + " " + user.getApellido());

	      return "user";
	   }
	   
}
