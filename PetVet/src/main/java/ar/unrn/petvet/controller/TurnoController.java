package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.DuenoMascota;
import ar.unrn.petvet.model.Persona;
import ar.unrn.petvet.model.TipoTurno;
import ar.unrn.petvet.model.Turno;
import ar.unrn.petvet.service.IDuenoService;
import ar.unrn.petvet.service.ITipoTurnoService;
import ar.unrn.petvet.service.ITurnoService;
import ar.unrn.petvet.service.jpa.VeterinariaServiceJPA;

@Controller
@RequestMapping("/turno/")
public class TurnoController {

	@Autowired
	private ITurnoService repo;
	@Autowired
	private ITipoTurnoService repoTipo;
	@Autowired
	private IDuenoService repoDuenoM;
	@Autowired
	private VeterinariaServiceJPA repoVet;

	@GetMapping("/")
	public String mostrarIndex(Model model, HttpServletRequest request) {
		List<Turno> turnos = repo.buscarTodos();
		model.addAttribute("turnos", turnos);
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "turno/listaTurno";
	}

	@PostMapping("/alta")
	public String alta(@ModelAttribute Turno turno2, Model model, HttpServletRequest request) {
		System.out.println("mica " + turno2.toString());
		model.addAttribute("titulo", "Alta de turno");
		//session
		HttpSession session = request.getSession(true);
		Persona p = (Persona) session.getAttribute("user");
		if (p == null)
			return "login";
		session.setAttribute("user", p);
		
		List<TipoTurno> tipos = repoTipo.buscarTodos();
		model.addAttribute("tipos", tipos);
		Turno t = new Turno();
		t.setTipoTurno(new TipoTurno());
		t.setDuenoMascota(repoDuenoM.buscarPorId(turno2.getDuenoMascota().getId()));
		t.setVete(repoVet.buscarPorId(turno2.getVete().getId()));
		t.setFecha(turno2.getFecha());
		t.setHora(turno2.getHora());
		model.addAttribute("turno", t);
		return "turno/datosTurno";
	}

	@GetMapping("/modificar/{id}")
	public String modificar(@PathVariable("id") int idTurno, Model model, HttpServletRequest request) {
		model.addAttribute("titulo", "Modificar el Turno");
		Turno t = repo.buscarPorId(idTurno);
		t.setDuenoMascota(repoDuenoM.buscarPorId(t.getDuenoMascota().getId()));
		t.setTipoTurno(repoTipo.buscarPorId(t.getTipoTurno().getId()));
		
		List<DuenoMascota> due�os = repoDuenoM.buscarTodos();
		model.addAttribute("duenosMascotas", due�os);
		List<TipoTurno> tipos = repoTipo.buscarTodos();
		model.addAttribute("tipos", tipos);
		
		System.out.println("turno controller " + t);
		model.addAttribute("turno", t);
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "turno/datosTurno";
	}	

	@PostMapping("/save")
	public String guardar(@ModelAttribute Turno turno,
			BindingResult result, Model model, RedirectAttributes attributes, HttpServletRequest request) {
		
		System.out.println("turno controller: " + turno);
		turno.setTipoTurno(repoTipo.buscarPorId(turno.getTipoTurno().getId()));
		turno.setDuenoMascota(repoDuenoM.buscarPorId(turno.getDuenoMascota().getId()));
		turno.setVete(repoVet.buscarPorId(turno.getVete().getId()));
		
		repo.insertar(turno);
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return ("redirect:/grilla/calen/" + turno.getVete().getId());
	}
	
	@GetMapping("/{id}")
	public String consulta(@PathVariable("id") int idTurno, @ModelAttribute Turno turno, Model model, HttpServletRequest request) {
		repo.buscarPorId(idTurno);
		turno.setDuenoMascota(repoDuenoM.buscarPorId(turno.getDuenoMascota().getId()));
		turno.setTipoTurno(repoTipo.buscarPorId(turno.getTipoTurno().getId()));
		model.addAttribute("turno", turno);
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "turno/consultaTurno";
	}
	
	@GetMapping("/eliminar/{id}")
	public String eliminar(@ModelAttribute("turno") Turno turno, Model model, BindingResult result,
			RedirectAttributes attributes, HttpServletRequest request) {
		repo.eliminar(turno.getId());
		attributes.addFlashAttribute("msg", "El registro fue guardado");
		//session
		HttpSession session = request.getSession(true);
		session.setAttribute("user", session.getAttribute("user"));
		return "redirect:/turno/";
	}

}
