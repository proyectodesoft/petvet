package ar.unrn.petvet.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import ar.unrn.petvet.model.Direccion;
import ar.unrn.petvet.model.Local;
import ar.unrn.petvet.service.jpa.LocalServiceJPA;

@Controller
@RequestMapping("/local/")
public class LocalController {
	
	@Autowired
	LocalServiceJPA repo;
	
	@GetMapping("/")
	public String lista(Model model) {
		List<Local> locales = repo.buscarTodos();
		model.addAttribute("locales", locales);
		return "local/listaLocales";
	}
	
	@GetMapping("/alta")
	public String alta(Model model, @ModelAttribute Local local) {
		model.addAttribute("titulo", "Alta de Tienda de Mascotas");
		Local l = new Local();
		model.addAttribute("local", l);
		Direccion dir = new Direccion();
		model.addAttribute("direccion", dir);
		return "local/datosLocal";
	}
	
	@GetMapping("/modificar/{id}")
	public String modificar(@PathVariable("id") int id, Model model) {
		model.addAttribute("titulo", "Modificar Tienda de Mascotas");
		Local l = repo.buscarPorId(id);
		model.addAttribute("local", l);
		model.addAttribute("direccion", l.getDireccion());
		return "local/datosLocal";
	}
	
	@PostMapping("/save")
	public String guardar(@ModelAttribute("local") Local local, @ModelAttribute("direccion") Direccion direccion, BindingResult result, RedirectAttributes attributes,
			HttpServletRequest request
			) {
		local.setDireccion(direccion);
		repo.insertar(local);
    	attributes.addFlashAttribute("msg", "El registro fue guardado");		
		return "redirect:/local/";
	}
}
